# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
import os
import unittest
import zipfile
from datetime import datetime
from io import BytesIO, StringIO
from typing import Optional
from unittest import mock
from unittest.mock import MagicMock

import boto3
import pytest
from botocore.response import StreamingBody
from dateutil.tz import tzutc
from pytest import fixture, mark
from pytest_mock import MockerFixture

from mlcvzoo_util.adapters.s3.adapter import S3Adapter
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation, S3Tuple
from mlcvzoo_util.tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


@fixture(name="s3_files")
def get_s3_files_fixture():
    """Return a s3 response with dummy s3 files."""
    return [
        {
            "ResponseMetadata": {
                "RequestId": "1749E48BEB07EFC5",
                "HostId": "",
                "HTTPStatusCode": 200,
                "HTTPHeaders": {
                    "accept-ranges": "bytes",
                    "content-length": "694",
                    "content-security-policy": "block-all-mixed-content",
                    "content-type": "application/xml",
                    "server": "s3",
                    "strict-transport-security": "max-age=31536000; includeSubDomains",
                    "vary": "Origin, Accept-Encoding",
                    "x-amz-bucket-region": "eu-central-1",
                    "x-amz-request-id": "1749E48BEB07EFC5",
                    "x-content-type-options": "nosniff",
                    "x-xss-protection": "1; mode=block",
                    "date": "Mon, 06 Mar 2023 17:27:05 GMT",
                    "set-cookie": "3b030adc0366ddb7aecd60a4ff017306=5ab7b0c5034fa99ea424d2bb5bd92ea9; "
                    "path=/; HttpOnly; Secure; SameSite=None",
                    "cache-control": "private",
                },
                "RetryAttempts": 0,
            },
            "IsTruncated": False,
            "Contents": [
                {
                    "Key": "test-bucket/test_file_1.txt",
                    "LastModified": datetime(2023, 3, 6, 17, 25, 42, 91000),
                    "ETag": '"2f50e06ff9729cf41b81112d48c3c1c4"',
                    "Size": 7694953,
                    "StorageClass": "STANDARD",
                    "Owner": {
                        "DisplayName": "s3",
                        "ID": "02d6176db174dc93cb1b899f7c6078f08654445fe8cf1b6ce98d8855f66bdbf4",
                    },
                }
            ],
            "Name": "bucket",
            "Prefix": "test-bucket/test_file_1.txt",
            "Delimiter": "",
            "MaxKeys": 1000,
            "EncodingType": "url",
            "KeyCount": 1,
        },
        {
            "ResponseMetadata": {
                "RequestId": "1749E48BEB07EFC5",
                "HostId": "",
                "HTTPStatusCode": 200,
                "HTTPHeaders": {
                    "accept-ranges": "bytes",
                    "content-length": "694",
                    "content-security-policy": "block-all-mixed-content",
                    "content-type": "application/xml",
                    "server": "s3",
                    "strict-transport-security": "max-age=31536000; includeSubDomains",
                    "vary": "Origin, Accept-Encoding",
                    "x-amz-bucket-region": "eu-central-1",
                    "x-amz-request-id": "1749E48BEB07EFC5",
                    "x-content-type-options": "nosniff",
                    "x-xss-protection": "1; mode=block",
                    "date": "Mon, 06 Mar 2023 17:27:05 GMT",
                    "set-cookie": "3b030adc0366ddb7aecd60a4ff017306=5ab7b0c5034fa99ea424d2bb5bd92ea9; "
                    "path=/; HttpOnly; Secure; SameSite=None",
                    "cache-control": "private",
                },
                "RetryAttempts": 0,
            },
            "IsTruncated": False,
            "Contents": [
                {
                    "Key": "test-bucket/test_file_2.txt",
                    "LastModified": datetime(2023, 3, 6, 17, 25, 42, 91000),
                    "ETag": '"2f50e06ff9729cf41b81112d48c3c1c4"',
                    "Size": 7694953,
                    "StorageClass": "STANDARD",
                    "Owner": {
                        "DisplayName": "s3",
                        "ID": "02d6176db174dc93cb1b899f7c6078f08654445fe8cf1b6ce98d8855f66bdbf4",
                    },
                }
            ],
            "Name": "bucket",
            "Prefix": "test-bucket/test_file_2.txt",
            "Delimiter": "",
            "MaxKeys": 1000,
            "EncodingType": "url",
            "KeyCount": 1,
        },
    ]


@fixture(scope="function")
def list_object_mock(mocker: MockerFixture, s3_files) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.adapters.s3.adapter.S3Adapter._list_objects",
        return_value=s3_files,
    )


@fixture(scope="function")
def list_object_no_files_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.adapters.s3.adapter.S3Adapter._list_objects",
        return_value=[
            {
                "ResponseMetadata": {
                    "RequestId": "1749E48BEB07EFC5",
                    "HostId": "",
                    "HTTPStatusCode": 200,
                    "HTTPHeaders": {
                        "accept-ranges": "bytes",
                        "content-length": "694",
                        "content-security-policy": "block-all-mixed-content",
                        "content-type": "application/xml",
                        "server": "s3",
                        "strict-transport-security": "max-age=31536000; includeSubDomains",
                        "vary": "Origin, Accept-Encoding",
                        "x-amz-bucket-region": "eu-central-1",
                        "x-amz-request-id": "1749E48BEB07EFC5",
                        "x-content-type-options": "nosniff",
                        "x-xss-protection": "1; mode=block",
                        "date": "Mon, 06 Mar 2023 17:27:05 GMT",
                        "set-cookie": "3b030adc0366ddb7aecd60a4ff017306=5ab7b0c5034fa99ea424d2bb5bd92ea9; path=/; HttpOnly; Secure; SameSite=None",
                        "cache-control": "private",
                    },
                    "RetryAttempts": 0,
                },
                "IsTruncated": False,
                "Contents": [],
                "Name": "bucket",
                "Prefix": "test-bucket/test_file.txt",
                "Delimiter": "",
                "MaxKeys": 1000,
                "EncodingType": "url",
                "KeyCount": 1,
            }
        ],
    )


@fixture(scope="function")
def download_file_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.adapters.s3.adapter.S3Adapter.download_file",
        return_value=("test_file.txt", "test-bucket/test_file.txt"),
    )


@fixture(scope="function")
def download_file_exception_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_util.adapters.s3.adapter.S3Adapter.download_file",
        side_effect=NotADirectoryError(),
    )


@fixture(name="is_file_fixture")
def is_file_fixture(mocker):
    return mocker.patch("os.path.isfile", return_value=True)


@fixture(name="paginator_mock")
def paginator_fixture(s3_files):
    paginator = MagicMock()
    paginator.paginate.return_value = s3_files
    return paginator


@fixture(name="s3_client")
def s3_client_fixture(paginator_mock):
    """Mock the s3 client of the S3Adapter with available s3 files."""

    mock_response_header = {
        "ResponseMetadata": {
            "RequestId": "2001E48BEB07EFC5",
            "HostId": "",
            "HTTPStatusCode": 200,
            "HTTPHeaders": {
                "etag": "456789",
            },
        }
    }

    def side_effect_s3_client_list_objects_v2(Bucket, Prefix):
        # Note: The S3 client uses capitalized arguments in its get_object call
        if Bucket == "test-bucket-files" and Prefix == "test-prefix-files":
            return {
                "ResponseMetadata": {
                    "RequestId": "17C9E37234560449",
                    "HostId": "",
                    "HTTPStatusCode": 200,
                    "HTTPHeaders": {
                        "accept-ranges": "bytes",
                        "content-length": "1354",
                        "content-security-policy": "block-all-mixed-content",
                        "content-type": "application/xml",
                        "server": "MinIO",
                        "strict-transport-security": "max-age=31536000; includeSubDomains",
                        "vary": "Origin, Accept-Encoding",
                        "x-amz-request-id": "17C9E37234560449",
                        "x-content-type-options": "nosniff",
                        "x-xss-protection": "1; mode=block",
                        "date": "Fri, 26 Apr 2024 17:06:52 GMT",
                    },
                    "RetryAttempts": 0,
                },
                "IsTruncated": False,
                "Contents": [
                    {
                        "Key": "key1",
                        "LastModified": datetime(
                            2024, 4, 24, 11, 30, 45, 131000, tzinfo=tzutc()
                        ),
                        "ETag": '"340419bd9d5d0dba94bc8f9a5abcb95c"',
                        "Size": 49288,
                        "StorageClass": "STANDARD",
                        "Owner": {
                            "DisplayName": "minio",
                            "ID": "id1",
                        },
                    },
                    {
                        "Key": "key2",
                        "LastModified": datetime(
                            2024, 4, 24, 11, 30, 45, 126000, tzinfo=tzutc()
                        ),
                        "ETag": '"575f862582a21a82d8bc739a00e81660"',
                        "Size": 48676,
                        "StorageClass": "STANDARD",
                        "Owner": {
                            "DisplayName": "minio",
                            "ID": "id2",
                        },
                    },
                ],
                "Name": "model-registry",
                "Prefix": "test-prefix",
                "Delimiter": "",
                "MaxKeys": 1000,
                "EncodingType": "url",
                "KeyCount": 2,
            }

        elif Bucket == "test-bucket-no-files" and Prefix == "test-prefix-no-files":
            return {
                "ResponseMetadata": {
                    "RequestId": "17C9E405E6A5FF5E",
                    "HostId": "",
                    "HTTPStatusCode": 200,
                    "HTTPHeaders": {
                        "accept-ranges": "bytes",
                        "content-length": "326",
                        "content-security-policy": "block-all-mixed-content",
                        "content-type": "application/xml",
                        "server": "MinIO",
                        "strict-transport-security": "max-age=31536000; includeSubDomains",
                        "vary": "Origin, Accept-Encoding",
                        "x-amz-request-id": "17C9E405E6A5FF5E",
                        "x-content-type-options": "nosniff",
                        "x-xss-protection": "1; mode=block",
                        "date": "Fri, 26 Apr 2024 17:17:27 GMT",
                    },
                    "RetryAttempts": 0,
                },
                "IsTruncated": False,
                "Name": "model-registry",
                "Prefix": "prefix",
                "Delimiter": "",
                "MaxKeys": 1000,
                "EncodingType": "url",
                "KeyCount": 0,
            }

        elif Bucket == "test-bucket-create-zip" and Prefix == "test-prefix-create-zip":
            return {
                "ResponseMetadata": {
                    "RequestId": "17C9E37234560449",
                    "HostId": "",
                    "HTTPStatusCode": 200,
                    "HTTPHeaders": {
                        "accept-ranges": "bytes",
                        "content-length": "1354",
                        "content-security-policy": "block-all-mixed-content",
                        "content-type": "application/xml",
                        "server": "MinIO",
                        "strict-transport-security": "max-age=31536000; includeSubDomains",
                        "vary": "Origin, Accept-Encoding",
                        "x-amz-request-id": "17C9E37234560449",
                        "x-content-type-options": "nosniff",
                        "x-xss-protection": "1; mode=block",
                        "date": "Fri, 26 Apr 2024 17:06:52 GMT",
                    },
                    "RetryAttempts": 0,
                },
                "IsTruncated": False,
                "Contents": [
                    {
                        "Key": "test-prefix-create-zip/first_image.jpg",
                        "LastModified": datetime(
                            2024, 4, 24, 11, 30, 45, 131000, tzinfo=tzutc()
                        ),
                        "ETag": '"340419bd9d5d0dba94bc8f9a5abcb95c"',
                        "Size": 49288,
                        "StorageClass": "STANDARD",
                        "Owner": {
                            "DisplayName": "minio",
                            "ID": "id1",
                        },
                    },
                    {
                        "Key": "test-prefix-create-zip/second_image.jpg",
                        "LastModified": datetime(
                            2024, 4, 24, 11, 30, 45, 126000, tzinfo=tzutc()
                        ),
                        "ETag": '"575f862582a21a82d8bc739a00e81660"',
                        "Size": 48676,
                        "StorageClass": "STANDARD",
                        "Owner": {
                            "DisplayName": "minio",
                            "ID": "id2",
                        },
                    },
                ],
                "Name": "model-registry",
                "Prefix": "test-prefix",
                "Delimiter": "",
                "MaxKeys": 1000,
                "EncodingType": "url",
                "KeyCount": 2,
            }

    def side_effect_s3_client_get_object(Bucket, Key):
        # Note: The S3 client uses capitalized arguments in its get_object call
        if (
            Bucket == "test-bucket-file-as-bytes"
            and Key == "test-prefix-files-as-bytes"
        ) or (Bucket == "test-bucket-files" and Key == "key1"):
            return {
                "Body": StreamingBody(
                    raw_stream=BytesIO(b"this is a file as bytes."),
                    content_length=len(b"this is a file as bytes."),
                )
            }

        elif (
            Bucket == "test-bucket-get-yaml-file-as-dict"
            and Key == "test-prefix-get-yaml-file-as-dict/config.yaml"
        ):

            return {
                "Body": StreamingBody(
                    raw_stream=BytesIO(b"{'random_yaml_key': 'random yaml value'}"),
                    content_length=len(b"{'random_yaml_key': 'random yaml value'}"),
                )
            }
        elif Bucket == "test-bucket-files" and Key == "key2":
            return {
                "Body": StreamingBody(
                    raw_stream=BytesIO(b""),
                    content_length=len(b""),
                ),
            }

    client = MagicMock()
    client.upload_file.return_value = None
    client.get_paginator.return_value = paginator_mock
    client.download_file.return_value = None
    client.get_object.side_effect = side_effect_s3_client_get_object
    client.head_object.return_value = mock_response_header
    client.list_objects_v2.side_effect = side_effect_s3_client_list_objects_v2
    client.generate_presigned_url.side_effect = [
        "some_presigned_url",
        "another_presigned_url",
    ]
    client.put_object.return_value = None
    return client


@fixture(name="boto3_client")
def boto3_client_fixture(mocker, s3_client):
    """Mock the boto3 client of the S3StorageAdapter."""
    return mocker.patch(
        "boto3.client",
        return_value=s3_client,
    )


class TestAdapter(TestTemplate):
    @mark.usefixtures(
        "list_object_mock",
        "download_file_mock",
    )
    def test_download_objects(self) -> None:
        """Test if objects can be successfully downloaded from S3 storage."""
        os_value = os.getenv(key="TEST_OUTPUT")

        os.environ["TEST_OUTPUT"] = os.path.join(self.project_root, "test_output")

        exception: Optional[Exception] = None

        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)

        s3_data_location = S3DataLocation(
            credentials=s3_credentials,
            uri="http://127.0.0.1:9000/test-bucket/test_file.txt",
            location_id="TEST_OUTPUT",
        )

        (
            string_replacement_map,
            downloaded_files,
            original_files,
        ) = s3_adapter.download_objects(
            bucket=s3_data_location.bucket,
            prefix=s3_data_location.prefix,
            location_id=s3_data_location.location_id,
            string_replacement_map={},
        )
        try:
            assert string_replacement_map == {
                "TEST_OUTPUT": os.path.join(self.project_root, "test_output")
            }
            assert len(downloaded_files) == 2
            assert downloaded_files[0] == "test_file.txt"
            assert downloaded_files[1] == "test_file.txt"
            assert original_files[0] == "test-bucket/test_file.txt"
            assert original_files[1] == "test-bucket/test_file.txt"
        except Exception as e:
            exception = e

        if os_value is None:
            os.environ.pop("TEST_OUTPUT")
        else:
            os.environ["TEST_OUTPUT"] = os_value

        if exception is not None:
            raise exception

    @mark.usefixtures(
        "list_object_mock",
        "download_file_exception_mock",
    )
    def test_download_objects_exception(self) -> None:
        """Test if exception is handled correctly trying to download objects from S3 storage."""
        os_value = os.getenv(key="TEST_OUTPUT")

        os.environ["TEST_OUTPUT"] = os.path.join(self.project_root, "test_output")

        exception: Optional[Exception] = None
        try:
            s3_credentials = S3Credentials(
                s3_endpoint_url="http://127.0.0.1:9000",
                s3_region_name="eu-central-1",
                s3_artifact_access_key="testuser",
                s3_artifact_access_id="12345678",
            )
            s3_adapter = S3Adapter(credentials=s3_credentials)

            s3_data_location = S3DataLocation(
                credentials=s3_credentials,
                uri="http://127.0.0.1:9000/test-bucket/test_file.txt",
                location_id="TEST_OUTPUT",
            )

            (
                string_replacement_map,
                downloaded_files,
                original_files,
            ) = s3_adapter.download_objects(
                bucket=s3_data_location.bucket,
                prefix=s3_data_location.prefix,
                location_id=s3_data_location.location_id,
                string_replacement_map={},
            )

            assert string_replacement_map == {
                "TEST_OUTPUT": os.path.join(self.project_root, "test_output")
            }
            assert len(downloaded_files) == 0
        except Exception as e:
            exception = e

        if os_value is None:
            os.environ.pop("TEST_OUTPUT")
        else:
            os.environ["TEST_OUTPUT"] = os_value

        if exception is not None:
            raise exception

    @mark.usefixtures(
        "list_object_no_files_mock",
        "download_file_mock",
    )
    def test_download_objects_no_files(self) -> None:
        """Test for handling download from empty 'directories' in the S3 storage."""
        os_value = os.getenv(key="TEST_OUTPUT")

        os.environ["TEST_OUTPUT"] = os.path.join(self.project_root, "test_output")

        exception: Optional[Exception] = None
        try:
            s3_credentials = S3Credentials(
                s3_endpoint_url="http://127.0.0.1:9000",
                s3_region_name="eu-central-1",
                s3_artifact_access_key="testuser",
                s3_artifact_access_id="12345678",
            )
            s3_adapter = S3Adapter(credentials=s3_credentials)

            s3_data_location = S3DataLocation(
                credentials=s3_credentials,
                uri="http://127.0.0.1:9000/test-bucket/test_file.txt",
                location_id="TEST_OUTPUT",
            )

            (
                string_replacement_map,
                downloaded_files,
                original_files,
            ) = s3_adapter.download_objects(
                bucket=s3_data_location.bucket,
                prefix=s3_data_location.prefix,
                location_id=s3_data_location.location_id,
                string_replacement_map={},
            )

            assert string_replacement_map == {
                "TEST_OUTPUT": os.path.join(self.project_root, "test_output")
            }
            assert len(downloaded_files) == 0
        except Exception as e:
            exception = e

        if os_value is None:
            os.environ.pop("TEST_OUTPUT")
        else:
            os.environ["TEST_OUTPUT"] = os_value

        if exception is not None:
            raise exception

    @mark.usefixtures("boto3_client", "is_file_fixture")
    def test_upload_file(self) -> None:
        """Test uploading a file to the S3 storage."""
        s3_adapter = S3Adapter(
            credentials=S3Credentials(
                s3_endpoint_url="http://127.0.0.1:9000",
                s3_region_name="eu-central-1",
                s3_artifact_access_key="testuser",
                s3_artifact_access_id="12345678",
            )
        )

        local_file_path = os.path.join(
            self.project_root, "test_data/checkpoints/model.pth"
        )
        s3_adapter.upload_file(
            local_path=local_file_path,
            bucket="test-bucket",
            key="checkpoints/result_model.pth",
        )

        assert s3_adapter._client.upload_file.call_count == 1
        assert s3_adapter._client.upload_file.call_args == mock.call(
            Bucket="test-bucket",
            Key="checkpoints/result_model.pth",
            Filename=local_file_path,
        )

    def test_s3_data_init_error(self) -> None:
        """Test if invalid uri format raises ValueError."""
        with self.assertRaises(ValueError):
            s3_credentials = S3Credentials(
                s3_endpoint_url="http://127.0.0.1:9000",
                s3_region_name="eu-central-1",
                s3_artifact_access_key="testuser",
                s3_artifact_access_id="12345678",
            )
            S3DataLocation(uri="http://127.0.0.1:9000", credentials=s3_credentials)

    @mark.usefixtures("boto3_client")
    def test_get_file_as_bytes(self):
        """Test if a binary encoded file is returned."""

        s3_adapter = S3Adapter(
            credentials=S3Credentials(
                s3_endpoint_url="http://127.0.0.1:9000",
                s3_region_name="eu-central-1",
                s3_artifact_access_key="testuser",
                s3_artifact_access_id="12345678",
            )
        )
        bucket = "test-bucket-file-as-bytes"
        prefix = "test-prefix-files-as-bytes"
        file_as_bytes = s3_adapter.get_file_as_bytes(bucket=bucket, prefix=prefix)

        # check return value
        self.assertEqual(b"this is a file as bytes.", file_as_bytes)

        # check get_object call
        self.assertEqual(1, s3_adapter._client.get_object.call_count)
        self.assertEqual(
            {
                "Bucket": bucket,
                "Key": prefix,
            },
            s3_adapter._client.get_object.call_args.kwargs,
        )

    @mark.usefixtures("boto3_client")
    def test_get_file_hash(self):
        """Test if a file hash is returned."""
        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)

        s3_data_location = S3DataLocation(
            credentials=s3_credentials,
            uri="http://127.0.0.1:9000/test-bucket/test_file.txt",
            location_id="TEST_OUTPUT",
        )

        hash_value = s3_adapter.get_file_hash(
            bucket=s3_data_location.bucket, prefix=s3_data_location.prefix
        )

        # check return value
        self.assertEqual("456789", hash_value)

        # check head_object call
        self.assertEqual(1, s3_adapter._client.head_object.call_count)
        self.assertEqual(
            {"Bucket": "test-bucket", "Key": "test_file.txt"},
            s3_adapter._client.head_object.call_args.kwargs,
        )

    @mark.usefixtures("boto3_client")
    def test_get_yaml_file_as_json_string(self):
        """Test if a config as a json formatted string is returned."""

        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        bucket = "test-bucket-get-yaml-file-as-dict"
        prefix = "test-prefix-get-yaml-file-as-dict/config.yaml"
        json_string = s3_adapter.get_yaml_file_as_json_string(
            bucket=bucket, prefix=prefix
        )

        # check return value
        self.assertEqual('{"random_yaml_key": "random yaml value"}', json_string)

        # check get_object calls
        self.assertEqual(1, s3_adapter._client.get_object.call_count)
        self.assertEqual(
            {"Bucket": bucket, "Key": prefix},
            s3_adapter._client.get_object.call_args.kwargs,
        )

    def test_get_yaml_file_as_dict_fails(self):
        """Test if a RuntimeError is raised if the provided config is not a yaml file."""
        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        bucket = "test-bucket"
        prefix = "test-prefix/not_a_yaml_config.json"

        with self.assertRaises(RuntimeError):
            s3_adapter.get_yaml_file_as_dict(bucket=bucket, prefix=prefix)

    @mark.usefixtures("boto3_client")
    def test_get_yaml_file_as_dict(self):
        """Test if a config stored in a yaml file is returned as a dict."""
        bucket = "test-bucket-get-yaml-file-as-dict"
        prefix = "test-prefix-get-yaml-file-as-dict/config.yaml"

        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        config_dict = s3_adapter.get_yaml_file_as_dict(bucket=bucket, prefix=prefix)

        # check return value
        self.assertEqual({"random_yaml_key": "random yaml value"}, config_dict)

    @mark.usefixtures("boto3_client")
    def test_list_files(self):
        """Test if list of files for a given S3 prefix is returned."""
        bucket = "test-bucket-files"
        prefix = "test-prefix-files"
        files_expected = [
            S3Tuple(bucket=bucket, prefix="key1"),
            S3Tuple(bucket=bucket, prefix="key2"),
        ]

        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        files_actual = s3_adapter.list_files(bucket=bucket, prefix=prefix)
        self.assertEqual(files_expected, files_actual)
        self.assertEqual(1, s3_adapter._client.list_objects_v2.call_count)
        self.assertEqual(
            {"Bucket": "test-bucket-files", "Prefix": "test-prefix-files"},
            s3_adapter._client.list_objects_v2.call_args_list[0].kwargs,
        )

        @mark.usefixtures("boto3_client")
        def test_list_files_filter_empty_files(self):
            """Test if list of non-empty files for a given S3 prefix is returned."""
            bucket = "test-bucket-files"
            prefix = "test-prefix-files"
            files_expected = [
                S3Tuple(bucket=bucket, prefix="key1"),
            ]

            s3_credentials = S3Credentials(
                s3_endpoint_url="http://127.0.0.1:9000",
                s3_region_name="eu-central-1",
                s3_artifact_access_key="testuser",
                s3_artifact_access_id="12345678",
            )
            s3_adapter = S3Adapter(credentials=s3_credentials)
            files_actual = s3_adapter.list_files(
                bucket=bucket, prefix=prefix, filter_empty_files=True
            )
            self.assertEqual(files_expected, files_actual)
            self.assertEqual(1, s3_adapter._client.list_objects_v2.call_count)
            self.assertEqual(
                {"Bucket": "test-bucket-files", "Prefix": "test-prefix-files"},
                s3_adapter._client.list_objects_v2.call_args_list[0].kwargs,
            )

    @mark.usefixtures("boto3_client")
    def test_list_files_no_files_for_prefix(self):
        """Test if empty list for a given S3 prefix with no files is returned."""

        bucket = "test-bucket-no-files"
        prefix = "test-prefix-no-files"
        files_expected = []

        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        files_actual = s3_adapter.list_files(bucket=bucket, prefix=prefix)
        self.assertEqual(files_expected, files_actual)
        self.assertEqual(1, s3_adapter._client.list_objects_v2.call_count)
        self.assertEqual(
            {"Bucket": "test-bucket-no-files", "Prefix": "test-prefix-no-files"},
            s3_adapter._client.list_objects_v2.call_args_list[0].kwargs,
        )

    @mark.usefixtures("boto3_client")
    def test_put_image_to_s3_bytes(self):
        """Test if a bytes encoded image is put to s3."""
        bucket = "test-bucket"
        prefix = "test-prefix"

        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        s3_adapter.put_file(
            file=b"this is a bytes encoded image",
            bucket=bucket,
            prefix=prefix,
        )

        # check put_object call
        self.assertEqual(1, s3_adapter._client.put_object.call_count)
        self.assertEqual(
            {
                "Body": b"this is a bytes encoded image",
                "Bucket": "test-bucket",
                "Key": "test-prefix",
            },
            s3_adapter._client.put_object.call_args.kwargs,
        )

    @mark.usefixtures("boto3_client")
    def test_put_image_to_s3_string(self):
        """Test if a string encoded file is put to S3."""
        bucket = "test-bucket"
        prefix = "test-prefix"

        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        s3_adapter.put_file(
            file="this is a string encoded file",
            bucket=bucket,
            prefix=prefix,
        )

        # check put_object call
        self.assertEqual(1, s3_adapter._client.put_object.call_count)
        self.assertEqual(
            {
                "Body": "this is a string encoded file",
                "Bucket": "test-bucket",
                "Key": "test-prefix",
            },
            s3_adapter._client.put_object.call_args.kwargs,
        )

    @mark.usefixtures("boto3_client")
    def test_generate_presigned_urls(self):
        """Test if a list of presigned_urls are returned."""
        bucket1 = "test-bucket"
        prefix1 = "test-prefix"
        bucket2 = "another-test-bucket"
        prefix2 = "another-test-prefix"

        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        urls = s3_adapter.generate_presigned_urls(
            files=[
                S3Tuple(bucket=bucket1, prefix=prefix1),
                S3Tuple(bucket=bucket2, prefix=prefix2),
            ]
        )

        # check return value
        self.assertEqual(["some_presigned_url", "another_presigned_url"], urls)

        # check generate_presigned_url call
        self.assertEqual(2, s3_adapter._client.generate_presigned_url.call_count)
        self.assertEqual(
            {
                "ClientMethod": "get_object",
                "Params": {"Bucket": "test-bucket", "Key": "test-prefix"},
            },
            s3_adapter._client.generate_presigned_url.call_args_list[0].kwargs,
        )
        self.assertEqual(
            {
                "ClientMethod": "get_object",
                "Params": {
                    "Bucket": "another-test-bucket",
                    "Key": "another-test-prefix",
                },
            },
            s3_adapter._client.generate_presigned_url.call_args_list[1].kwargs,
        )

    @mark.usefixtures("boto3_client")
    @mock.patch("zipfile.ZipFile")
    def test_create_downloadable_zip_of_files(self, mock_zip):
        """Test if download link for zipped file is returned."""
        mock_zip.write.return_value = None
        bucket = "test-bucket-create-zip"
        prefix = "test-prefix-create-zip"
        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)
        url = s3_adapter.create_downloadable_zip_of_files(
            zipfile_name="test_file.zip",
            bucket=bucket,
            key=prefix,
        )

        # check return value
        self.assertEqual("some_presigned_url", url)

        # check list_objects_v2 call
        self.assertEqual(1, s3_adapter._client.list_objects_v2.call_count)
        self.assertEqual(
            {"Bucket": bucket, "Prefix": prefix},
            s3_adapter._client.list_objects_v2.call_args.kwargs,
        )

        # check first download_file call
        self.assertEqual(2, s3_adapter._client.download_file.call_count)
        self.assertEqual(
            bucket,
            s3_adapter._client.download_file.call_args_list[0].kwargs["Bucket"],
        )
        self.assertEqual(
            f"{prefix}/first_image.jpg",
            s3_adapter._client.download_file.call_args_list[0].kwargs["Key"],
        )
        self.assertEqual(
            "first_image.jpg",
            os.path.split(
                s3_adapter._client.download_file.call_args_list[0].kwargs["Filename"]
            )[
                -1
            ],  # head of path is random due to temp file so just check correct name of file
        )

        self.assertEqual(
            bucket,
            s3_adapter._client.download_file.call_args_list[1].kwargs["Bucket"],
        )
        self.assertEqual(
            f"{prefix}/second_image.jpg",
            s3_adapter._client.download_file.call_args_list[1].kwargs["Key"],
        )
        self.assertEqual(
            "second_image.jpg",
            os.path.split(
                s3_adapter._client.download_file.call_args_list[1].kwargs["Filename"]
            )[
                -1
            ],  # head of path is random due to temp file so just check correct name of file
        )

        # check upload_file call
        self.assertEqual(1, s3_adapter._client.upload_file.call_count)
        self.assertEqual(
            bucket,
            s3_adapter._client.upload_file.call_args_list[0].kwargs["Bucket"],
        )
        self.assertEqual(
            f"{prefix}/test_file.zip",
            s3_adapter._client.upload_file.call_args_list[0].kwargs["Key"],
        )
        self.assertEqual(
            "test_file.zip",
            os.path.split(
                s3_adapter._client.upload_file.call_args_list[0].kwargs["Filename"]
            )[
                -1
            ],  # head of path is random due to temp file so just check correct name of file
        )

    @mark.usefixtures("boto3_client")
    def test_create_downloadable_zip_of_files_fails(self):
        """Test if KeyError is raised if the provided files can not be found."""
        bucket = "test-bucket-no-files"
        prefix = "test-prefix-no-files"
        s3_credentials = S3Credentials(
            s3_endpoint_url="http://127.0.0.1:9000",
            s3_region_name="eu-central-1",
            s3_artifact_access_key="testuser",
            s3_artifact_access_id="12345678",
        )
        s3_adapter = S3Adapter(credentials=s3_credentials)

        with self.assertRaises(KeyError):
            url = s3_adapter.create_downloadable_zip_of_files(
                zipfile_name="test_file.zip", bucket=bucket, key=prefix
            )


if __name__ == "__main__":
    unittest.main()
