# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Define common fixtures for the unit tests."""

import os
import shutil
from logging import getLogger
from pathlib import Path

import pytest
import related
from attr import define
from mlcvzoo_base.api.configuration import ModelConfiguration

import mlcvzoo_util
from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials

logger = getLogger(__name__)


@define
class TestConfig(ModelConfiguration):
    test_config: str = related.StringField(required=False, default="Hello World")


@pytest.fixture(name="project_root")
def project_root_fixture() -> Path:
    """Provide the project root path."""

    this_dir = Path(os.path.dirname(os.path.abspath(__file__))).resolve()

    setup_path = this_dir
    while setup_path.exists() and setup_path.name != mlcvzoo_util.__name__:
        if setup_path == setup_path.parent:
            raise RuntimeError("Could not find setup_path!")
        else:
            setup_path = setup_path.parent
    # One more to be above the target directory
    setup_path = setup_path.parent

    return setup_path


@pytest.fixture(name="cleanup", scope="function", autouse=False)
def cleanup_fixture(project_root: Path):
    """Removes the data_output directory after the test is run."""

    yield  # Test runs now

    shutil.rmtree(project_root / "test_output", ignore_errors=True)


@pytest.fixture(name="test_config")
def test_config_fixture() -> TestConfig:
    """Provide a test model configuration."""
    return TestConfig(unique_name="test_unique_name")


@pytest.fixture(name="mlflow_credentials")
def mlflow_credentials_fixture() -> MlflowCredentials:
    return MlflowCredentials(
        server_uri="http://test-uri.com",
        s3_artifact_access_key="test_access_key",
        s3_artifact_access_id="test_access_id",
        s3_endpoint_url="http://test-artifact-uri.com",
        s3_region_name="eu-central-1",
    )


@pytest.fixture(name="mlflow_adapter")
def mlflow_adapter_fixture(mlflow_credentials) -> MlflowAdapter:
    """Provide a test model configuration."""
    return MlflowAdapter(credentials=mlflow_credentials)
