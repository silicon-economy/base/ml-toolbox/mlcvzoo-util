# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import os
import unittest

from mlcvzoo_util.adapters.s3.credentials import S3Credentials


class TestS3Credentials(unittest.TestCase):
    """Test class for S3Credentials."""

    def setUp(self) -> None:

        # Set dummy variables for test cases
        self.credentials = (
            "S3_SECRET_ACCESS_KEY",
            "S3_ACCESS_KEY_ID",
            "S3_ENDPOINT",
            "S3_REGION_NAME",
        )

        self.values = []
        for credential in self.credentials:
            value = os.getenv(credential)
            if value is not None:
                self.values.append(value)
            else:
                self.values.append(None)

            # Temporarily set dummy value for test case
            os.environ[credential] = credential.lower()

    def tearDown(self) -> None:
        # Reset variables to initial values
        for pair in zip(self.credentials, self.values):
            if pair[1] is not None:
                os.environ[pair[0]] = pair[1]
            else:
                del os.environ[pair[0]]

    def test_init_from_environment_succeeds(self):
        """
        Test if S3Credentials are initialized successfully if all environment variables are set.
        """

        credentials = S3Credentials.init_from_os_environment()

        self.assertEqual(os.getenv("S3_ENDPOINT"), credentials.s3_endpoint_url)
        self.assertEqual(
            os.getenv("S3_SECRET_ACCESS_KEY"), credentials.s3_artifact_access_key
        )
        self.assertEqual(
            os.getenv("S3_ACCESS_KEY_ID"), credentials.s3_artifact_access_id
        )
        self.assertEqual(os.getenv("S3_REGION_NAME"), credentials.s3_region_name)

    def test_init_from_environment_fails_no_s3_uri(self):
        """
        Test if ValueError is raised if s3_endpoint is not set.
        """

        actual_variable = os.getenv("S3_ENDPOINT")

        del os.environ["S3_ENDPOINT"]

        with self.assertRaises(ValueError):
            S3Credentials.init_from_os_environment()

        os.environ["S3_ENDPOINT"] = actual_variable

    def test_init_from_environment_fails_no_s3_access_key_id(self):
        """
        Test if ValueError is raised if s3_access_key_id is not set.
        """

        actual_variable = os.getenv("S3_ACCESS_KEY_ID")

        del os.environ["S3_ACCESS_KEY_ID"]

        with self.assertRaises(ValueError):
            S3Credentials.init_from_os_environment()

        os.environ["S3_ACCESS_KEY_ID"] = actual_variable

    def test_init_from_environment_fails_no_s3_secret_access_key(self):
        """
        Test if ValueError is raised if s3_secret_access_key is not set.
        """

        actual_variable = os.getenv("S3_SECRET_ACCESS_KEY")

        del os.environ["S3_SECRET_ACCESS_KEY"]

        with self.assertRaises(ValueError):
            S3Credentials.init_from_os_environment()

        os.environ["S3_SECRET_ACCESS_KEY"] = actual_variable

    def test_init_from_environment_fails_no_s3_artifact_region(self):
        """
        Tests if ValueError is raised if s3_artifact_region is not set.
        """

        actual_variable = os.getenv("S3_REGION_NAME")

        del os.environ["S3_REGION_NAME"]

        with self.assertRaises(ValueError):
            S3Credentials.init_from_os_environment()

        os.environ["S3_REGION_NAME"] = actual_variable

    def test_to_dict(self):
        """Test if S3Credentials can be converted to dict."""
        s3_dict_keys_expected = {
            "s3_endpoint_url": "s3_endpoint",
            "s3_region_name": "s3_region_name",
            "s3_artifact_access_id": "s3_access_key_id",
            "s3_artifact_access_key": "s3_secret_access_key",
        }

        s3_credentials = S3Credentials.init_from_os_environment()
        s3_dict_keys_actual = s3_credentials.to_dict()

        self.assertEqual(s3_dict_keys_expected, s3_dict_keys_actual)
