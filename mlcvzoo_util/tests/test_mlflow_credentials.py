# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import os
import unittest

from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.credentials import S3Credentials


class TestMLFlowCredentials(unittest.TestCase):
    """Test class for MLFlowCredentials."""

    def setUp(self) -> None:

        # Set dummy variables for test cases
        self.credentials = (
            "AWS_SECRET_ACCESS_KEY",
            "AWS_ACCESS_KEY_ID",
            "MLFLOW_S3_ENDPOINT_URL",
            "MLFLOW_SERVER_URL",
            "AWS_DEFAULT_REGION",
        )

        self.values = []
        for credential in self.credentials:
            value = os.getenv(credential)
            if value is not None:
                self.values.append(value)
            else:
                self.values.append(None)

            # Temporarily set dummy value for test case
            os.environ[credential] = credential.lower()

    def tearDown(self) -> None:
        # Reset variables to initial values
        for pair in zip(self.credentials, self.values):
            if pair[1] is not None:
                os.environ[pair[0]] = pair[1]
            else:
                del os.environ[pair[0]]

    def test_init_from_environment_succeeds(self):
        """
        Test if MLFLowCredentials are initialized successfully if all environment variables are set.
        """

        credentials = MlflowCredentials.init_from_os_environment()

        self.assertEqual(
            os.getenv("MLFLOW_S3_ENDPOINT_URL"), credentials.s3_endpoint_url
        )
        self.assertEqual(
            os.getenv("AWS_SECRET_ACCESS_KEY"), credentials.s3_artifact_access_key
        )
        self.assertEqual(
            os.getenv("AWS_ACCESS_KEY_ID"), credentials.s3_artifact_access_id
        )
        self.assertEqual(os.getenv("AWS_DEFAULT_REGION"), credentials.s3_region_name)
        self.assertEqual(os.getenv("MLFLOW_SERVER_URL"), credentials.server_uri)

    def test_init_from_environment_fails_no_s3_uri(self):
        """
        Test if ValueError is raised if mlflow_s3_endpoint is not set.
        """

        actual_variable = os.getenv("MLFLOW_S3_ENDPOINT_URL")

        del os.environ["MLFLOW_S3_ENDPOINT_URL"]

        with self.assertRaises(ValueError):
            MlflowCredentials.init_from_os_environment()

        os.environ["MLFLOW_S3_ENDPOINT_URL"] = actual_variable

    def test_init_from_environment_fails_no_s3_server_url(self):
        """
        Test if ValueError is raised if mlflow_server_uri is not set.
        """

        actual_variable = os.getenv("MLFLOW_SERVER_URL")

        del os.environ["MLFLOW_SERVER_URL"]

        with self.assertRaises(ValueError):
            MlflowCredentials.init_from_os_environment()

        os.environ["MLFLOW_SERVER_URL"] = actual_variable

    def test_init_from_environment_fails_no_s3_access_key_id(self):
        """
        Test if ValueError is raised if aws_access_key_id is not set.
        """

        actual_variable = os.getenv("AWS_ACCESS_KEY_ID")

        del os.environ["AWS_ACCESS_KEY_ID"]

        with self.assertRaises(ValueError):
            MlflowCredentials.init_from_os_environment()

        os.environ["AWS_ACCESS_KEY_ID"] = actual_variable

    def test_init_from_environment_fails_no_s3_artifact_region(self):
        """
        Test if ValueError is raised if aws_region_name is not set.
        """

        actual_variable = os.getenv("AWS_DEFAULT_REGION")

        del os.environ["AWS_DEFAULT_REGION"]

        with self.assertRaises(ValueError):
            MlflowCredentials.init_from_os_environment()

        os.environ["AWS_DEFAULT_REGION"] = actual_variable

    def test_init_from_environment_fails_no_s3_secret_access_key(self):
        """
        Test if ValueError is raised if aws_secret_access_key is not set.
        """

        actual_variable = os.getenv("AWS_SECRET_ACCESS_KEY")

        del os.environ["AWS_SECRET_ACCESS_KEY"]

        with self.assertRaises(ValueError):
            S3Credentials.init_from_os_environment()

        os.environ["AWS_SECRET_ACCESS_KEY"] = actual_variable

    def test_update_os_environment(self):
        """
        Test if os variables are updated successfully.
        """

        credentials = MlflowCredentials(
            s3_artifact_access_key="key",
            s3_artifact_access_id="id",
            s3_endpoint_url="uri",
            s3_region_name="region",
            server_uri="server",
        )

        credentials.update_os_environment()

        self.assertEqual(os.getenv("MLFLOW_S3_ENDPOINT_URL"), "uri")
        self.assertEqual(os.getenv("AWS_SECRET_ACCESS_KEY"), "key")
        self.assertEqual(os.getenv("AWS_ACCESS_KEY_ID"), "id")
        self.assertEqual(os.getenv("AWS_DEFAULT_REGION"), "region")
        self.assertEqual(os.getenv("MLFLOW_SERVER_URL"), "server")

    def test_to_dict(self):
        """Test if MlflowCredentials can be converted to dict."""

        mlflow_dict_keys_expected = {
            "run_id": None,
            "s3_artifact_access_key": "aws_secret_access_key",
            "s3_artifact_access_id": "aws_access_key_id",
            "s3_endpoint_url": "mlflow_s3_endpoint_url",
            "server_uri": "mlflow_server_url",
            "s3_region_name": "aws_default_region",
        }

        mlflow_credentials = MlflowCredentials.init_from_os_environment()
        mlflow_dict_keys_actual = mlflow_credentials.to_dict()

        self.assertEqual(mlflow_dict_keys_expected, mlflow_dict_keys_actual)

    def test_to_s3_credentials(self):
        """Test if MlflowCredentials can be converted to S3Credentials."""
        mlflow_credentials = MlflowCredentials.init_from_os_environment()
        s3_credentials_expected = S3Credentials(
            s3_artifact_access_key="aws_secret_access_key",
            s3_artifact_access_id="aws_access_key_id",
            s3_endpoint_url="mlflow_s3_endpoint_url",
            s3_region_name="aws_default_region",
        )

        s3_credentials_actual = mlflow_credentials.to_s3_credentials()

        self.assertEqual(s3_credentials_expected, s3_credentials_actual)
