# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for the MlflowConnector used to connect to the Mlflow server."""

import logging
import os
import shutil
import unittest
from unittest import mock
from unittest.mock import MagicMock, call, mock_open, patch

import cv2
import pytest
from mlflow.entities.metric import Metric
from mlflow.entities.model_registry.model_version import ModelVersion
from mlflow.entities.model_registry.registered_model import RegisteredModel
from mlflow.entities.param import Param
from mlflow.entities.run import Run
from mlflow.entities.run_data import RunData
from mlflow.entities.run_info import RunInfo
from mlflow.exceptions import MlflowException

from mlcvzoo_util.adapters.mlflow.adapter import MlflowAdapter
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.mlflow.structs import TrainingStatus
from mlcvzoo_util.adapters.s3.adapter import S3Adapter
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlcvzoo_util.adapters.s3.data_location import S3Data, S3DataLocation
from mlcvzoo_util.utils import get_root_dir

logger = logging.getLogger(__name__)

try:
    from mlflow import MlflowClient
except ImportError as import_error:
    logger.error(
        "ImportError: %s, will try to use 'mlflow.tracking import MlflowClient' instead"
        % (str(import_error))
    )
    from mlflow.tracking import MlflowClient


class TestMlflowAdapter(unittest.TestCase):
    """Test the MlflowAdapter."""

    def setUp(self) -> None:
        def generate_param_instances(params: dict[str, str]) -> list[Param]:
            param_instances = []
            for key in params:
                param_instances.append(Param(key=key, value=params[key]))
            return param_instances

        self.credentials = MlflowCredentials(
            server_uri="http://test-uri.com",
            s3_artifact_access_key="test_access_key",
            s3_artifact_access_id="test_access_id",
            s3_endpoint_url="http://test-artifact-uri.com",
            s3_region_name="eu-central-1",
        )
        self.set_credentials = True
        self.model_checkpoint_suffix = ".pth"
        self.adapter = MlflowAdapter(credentials=self.credentials)
        self.adapter._client = MagicMock(spec=MlflowClient)
        self.adapter._s3_adapter = MagicMock(spec=S3Adapter)
        self.mock_run = mock.MagicMock(spec=Run)

        self.dummy_metrics = [Metric(key="score", value="0.42", timestamp="", step="")]

        self.dummy_params = generate_param_instances(
            params={
                "epochs": 10,
                "batch_size": 2,
                "dataset": "test_dataset",
                "model": "test_model",
                "description": "test_description",
                TrainingStatus.NEW.name: "00:00:00",
            }
        )

        self.dummy_params_running = generate_param_instances(
            params={
                "epochs": 10,
                "batch_size": 2,
                "dataset": "test_dataset",
                "model": "test_model",
                "description": "test_description",
                TrainingStatus.NEW.name: "00:00:00",
                TrainingStatus.RUNNING.name: "00:00:01",
            }
        )
        self.dummy_params_crashed = generate_param_instances(
            params={
                "epochs": 10,
                "batch_size": 2,
                "dataset": "test_dataset",
                "model": "test_model",
                "description": "test_description",
                TrainingStatus.NEW.name: "00:00:00",
                TrainingStatus.RUNNING.name: "00:00:01",
                TrainingStatus.CRASHED.name: "00:00:02",
            }
        )
        self.dummy_params_finished = generate_param_instances(
            params={
                "epochs": 10,
                "batch_size": 2,
                "dataset": "test_dataset",
                "model": "test_model",
                "description": "test_description",
                TrainingStatus.NEW.name: "00:00:00",
                TrainingStatus.RUNNING.name: "00:00:01",
                TrainingStatus.CRASHED.name: "00:00:02",
                TrainingStatus.FINISHED.name: "00:00:03",
            }
        )

        self.dummy_params_with_multiple_datasets = generate_param_instances(
            params={
                "epochs": 10,
                "batch_size": 2,
                "datasets": "['test_dataset', 'test_dataset_2']",
                "model": "test_model",
                "description": "test_description",
            }
        )

        self.dummy_infos = Run(
            run_info=RunInfo(
                run_uuid="",
                experiment_id="",
                user_id="",
                status="",
                start_time="",
                end_time="",
                lifecycle_stage="",
                run_id="123456",
                artifact_uri="s3://dummy_bucket/path/to/dummy_file.txt",
            ),
            run_data=RunData(metrics=self.dummy_metrics, params=self.dummy_params),
        )

        self.dummy_infos_running = Run(
            run_info=RunInfo(
                run_uuid="",
                experiment_id="",
                user_id="",
                status="",
                start_time="",
                end_time="",
                lifecycle_stage="",
                run_id="123456",
                artifact_uri="s3://dummy_bucket/path/to/dummy_file.txt",
            ),
            run_data=RunData(
                metrics=self.dummy_metrics, params=self.dummy_params_running
            ),
        )

        self.dummy_infos_crashed = Run(
            run_info=RunInfo(
                run_uuid="",
                experiment_id="",
                user_id="",
                status="",
                start_time="",
                end_time="",
                lifecycle_stage="",
                run_id="123456",
                artifact_uri="s3://dummy_bucket/path/to/dummy_file.txt",
            ),
            run_data=RunData(
                metrics=self.dummy_metrics, params=self.dummy_params_crashed
            ),
        )
        self.dummy_infos_finished = Run(
            run_info=RunInfo(
                run_uuid="",
                experiment_id="",
                user_id="",
                status="",
                start_time="",
                end_time="",
                lifecycle_stage="",
                run_id="123456",
                artifact_uri="s3://dummy_bucket/path/to/dummy_file.txt",
            ),
            run_data=RunData(
                metrics=self.dummy_metrics, params=self.dummy_params_finished
            ),
        )

        self.dummy_infos_with_multiple_datasets = Run(
            run_info=RunInfo(
                run_uuid="",
                experiment_id="",
                user_id="",
                status="",
                start_time="",
                end_time="",
                lifecycle_stage="",
                run_id="123456",
            ),
            run_data=RunData(
                metrics=self.dummy_metrics,
                params=self.dummy_params_with_multiple_datasets,
            ),
        )

        self.dummy_model = RegisteredModel(
            name="dummy_model",
            latest_versions=[
                ModelVersion(
                    name="dummy_model", version=1, creation_timestamp=0, run_id=42
                )
            ],
            creation_timestamp=1690927200,  # 2023-08-02 00:00:00
            last_updated_timestamp=1690927200,  # 2023-08-02 00:00:00
        )
        self.dummy_model_2 = RegisteredModel(
            name="dummy_model",
            latest_versions=[
                ModelVersion(
                    name="dummy_model", version=1, creation_timestamp=0, run_id=66
                )
            ],
            last_updated_timestamp=1690927200,  # 2023-08-02 00:00:00
            creation_timestamp=1690927200,  # 2023-08-02 00:00:00
        )

        self.dummy_model_version = ModelVersion(
            name="dummy_model", version=1, creation_timestamp=0, run_id=42
        )

        self.dummy_model_version_2 = ModelVersion(
            name="dummy_model", version=1, creation_timestamp=0, run_id=66
        )

    def test_get_class_metrics_success(self) -> None:
        """Test retrieving class metrics successfully."""
        # Mocking the metrics data
        self.mock_run.data.metrics = {
            "0_loading_unit_TP_ALL": 10.0,
            "0_loading_unit_FP_ALL": 5.0,
            "0_loading_unit_AP_ALL": 0.99,
            "0_loading_unit_F1_ALL": 0.92,
            "0_loading_unit_FN_ALL": 0.0,
            "0_loading_unit_PR_ALL": 0.85,
            "0_loading_unit_RC_ALL": 1.0,
            "0_loading_unit_COUNT_ALL": 60.0,
            "1_pallet_TP_ALL": 10.0,
            "1_pallet_FP_ALL": 5.0,
            "1_pallet_AP_ALL": 0.99,
            "1_pallet_F1_ALL": 0.92,
            "1_pallet_FN_ALL": 0.0,
            "1_pallet_PR_ALL": 0.85,
            "1_pallet_RC_ALL": 1.0,
            "1_pallet_COUNT_ALL": 60.0,
            "some_metric": 123,  # Additional metric not ending with "_ALL"
        }

        expected_result = {
            "0_loading_unit": {
                "TP": "10.0",
                "FP": "5.0",
                "AP": "0.99",
                "F1": "0.92",
                "FN": "0.0",
                "PR": "0.85",
                "RC": "1.0",
                "COUNT": "60.0",
            },
            "1_pallet": {
                "TP": "10.0",
                "FP": "5.0",
                "AP": "0.99",
                "F1": "0.92",
                "FN": "0.0",
                "PR": "0.85",
                "RC": "1.0",
                "COUNT": "60.0",
            },
        }

        with mock.patch.object(
            self.adapter, "get_run_of_registered_model", return_value=self.mock_run
        ):
            with mock.patch.object(
                self.adapter._client,
                "get_metric_history",
                side_effect=[
                    [Metric(key="TP", value=10.0, timestamp=123, step=1)],
                    [Metric(key="FP", value=5.0, timestamp=123, step=1)],
                    [Metric(key="AP", value=0.99, timestamp=123, step=1)],
                    [Metric(key="F1", value=0.92, timestamp=123, step=1)],
                    [Metric(key="FN", value=0.0, timestamp=123, step=1)],
                    [Metric(key="PR", value=0.85, timestamp=123, step=1)],
                    [Metric(key="RC", value=1.0, timestamp=123, step=1)],
                    [Metric(key="COUNT", value=60.0, timestamp=123, step=1)],
                    [Metric(key="TP", value=10.0, timestamp=123, step=1)],
                    [Metric(key="FP", value=5.0, timestamp=123, step=1)],
                    [Metric(key="AP", value=0.99, timestamp=123, step=1)],
                    [Metric(key="F1", value=0.92, timestamp=123, step=1)],
                    [Metric(key="FN", value=0.0, timestamp=123, step=1)],
                    [Metric(key="PR", value=0.85, timestamp=123, step=1)],
                    [Metric(key="RC", value=1.0, timestamp=123, step=1)],
                    [Metric(key="COUNT", value=60.0, timestamp=123, step=1)],
                ],
            ):
                result = self.adapter.get_class_metrics("registered_model_name")
                self.assertEqual(result, expected_result)

    def test_get_class_metrics_empty_metrics(self) -> None:
        """Test handling empty metrics data."""
        # Mocking empty metrics data
        self.mock_run.data.metrics = {}

        with mock.patch.object(
            self.adapter, "get_run_of_registered_model", return_value=self.mock_run
        ):
            result = self.adapter.get_class_metrics("registered_model_name")
            self.assertEqual(result, {})

    def test_get_class_metrics_skip_non_all_metrics(self) -> None:
        """Test skipping non-"_ALL" ending metrics."""
        # Mocking the metrics data with a non-"_ALL" ending metric
        self.mock_run.data.metrics = {
            "some_metric": 123,
            "0_loading_unit_FP_ALL": 456,
        }

        # Expected result should only contain the metric ending with "_ALL"
        expected_result = {"0_loading_unit": {"FP": "456"}}

        with mock.patch.object(
            self.adapter, "get_run_of_registered_model", return_value=self.mock_run
        ):
            with mock.patch.object(
                self.adapter._client,
                "get_metric_history",
                return_value=[Metric(key="FP", value=456, timestamp=123, step=1)],
            ):
                result = self.adapter.get_class_metrics("registered_model_name")
                # Asserting that the non-"_ALL" metric is skipped
                self.assertEqual(expected_result, result)

    def test_get_class_metrics_exception_handling(self) -> None:
        """Test exception handling when retrieving metrics."""
        # Mocking an exception when retrieving metrics
        with mock.patch.object(
            self.adapter,
            "get_run_of_registered_model",
            side_effect=Exception("Some error"),
        ):
            # Asserting that the exception is raised
            with self.assertRaises(Exception) as context:
                self.adapter.get_class_metrics("registered_model_name")
            # Asserting the exception message
            self.assertEqual(str(context.exception), "Some error")

    @mock.patch("mlcvzoo_util.adapters.mlflow.adapter.MlflowClient")
    def test_get_model_infos_from_registry_trainingstate(
        self, mock_mlflow_client
    ) -> None:
        """Test if training states are correctly set and returned."""
        mock_mlflow_client.get_run.return_value = self.dummy_infos
        mock_mlflow_client.search_registered_models.return_value = [self.dummy_model]

        model_infos = self.adapter.get_model_infos_from_registry()

        for info in model_infos:
            self.assertEqual(TrainingStatus.NEW.name, info.training_state)

        mock_mlflow_client.return_value = self.dummy_infos_running
        model_infos = self.adapter.get_model_infos_from_registry()
        for info in model_infos:
            self.assertEqual(TrainingStatus.RUNNING.name, info.training_state)

        mock_mlflow_client.return_value = self.dummy_infos_crashed
        model_infos = self.adapter.get_model_infos_from_registry()
        for info in model_infos:
            self.assertEqual(TrainingStatus.CRASHED.name, info.training_state)

        mock_mlflow_client.return_value = self.dummy_infos_finished
        model_infos = self.adapter.get_model_infos_from_registry()
        for info in model_infos:
            self.assertEqual(TrainingStatus.FINISHED.name, info.training_state)

    def test_get_model_infos_from_registry(self) -> None:
        """Test if list of ModelInfos is returned."""
        self.adapter._client.get_run.return_value = self.dummy_infos
        self.adapter._client.search_registered_models.return_value = [
            self.dummy_model,
            self.dummy_model_2,
        ]

        model_infos = self.adapter.get_model_infos_from_registry()

        # check search_registered_models call
        self.assertEqual(1, self.adapter._client.search_registered_models.call_count)
        self.assertEqual(
            {"filter_string": None},
            self.adapter._client.search_registered_models.call_args.kwargs,
        )

        # check mlflow.get_run call
        self.assertEqual(2, self.adapter._client.get_run.call_count)
        self.assertEqual(
            {"run_id": 42}, self.adapter._client.get_run.call_args_list[0].kwargs
        )
        self.assertEqual(
            {"run_id": 66}, self.adapter._client.get_run.call_args_list[1].kwargs
        )

        for info in model_infos:
            self.assertEqual("0.42", info.score)
            self.assertEqual(2, info.batch_size)
            self.assertEqual(["test_dataset"], info.datasets)
            self.assertEqual("test_description", info.description)
            self.assertEqual(10, info.epochs)
            self.assertEqual("", info.model_type_name)
            self.assertEqual("", info.baseline_model)
            self.assertEqual("dummy_model", info.name)
            self.assertEqual("123456", info.run_id)

    def test_get_model_info_from_registry_multiple_datasets(self) -> None:
        """Test if list of single ModelInfo is returned and the two datasets are parsed correctly."""

        self.adapter._client.search_registered_models.return_value = [self.dummy_model]
        self.adapter._client.get_run.return_value = (
            self.dummy_infos_with_multiple_datasets
        )

        model_infos = self.adapter.get_model_infos_from_registry()

        # check search_registered_models call
        self.assertEqual(1, self.adapter._client.search_registered_models.call_count)
        self.assertEqual(
            {"filter_string": None},
            self.adapter._client.search_registered_models.call_args.kwargs,
        )

        # check mlflow.get_run call
        self.assertEqual(1, self.adapter._client.get_run.call_count)
        self.assertEqual({"run_id": 42}, self.adapter._client.get_run.call_args.kwargs)

        for info in model_infos:
            self.assertEqual("0.42", info.score)
            self.assertEqual(2, info.batch_size)
            self.assertEqual(["test_dataset", "test_dataset_2"], info.datasets)
            self.assertEqual("test_description", info.description)
            self.assertEqual(10, info.epochs)
            self.assertEqual("", info.baseline_model)
            self.assertEqual("", info.algorithm_type)
            self.assertEqual("dummy_model", info.name)
            self.assertEqual("123456", info.run_id)

    @patch("mlflow.store.artifact.artifact_repo.ArtifactRepository")
    @patch("mlcvzoo_util.adapters.mlflow.adapter.get_artifact_repository")
    def test_delete_model_from_registry(
        self, get_artifact_repository_mock, artifact_repository_mock
    ) -> None:
        """Test if model is deleted successfully.

        Since the success of the function mostly depends on the Mlflow functions, this test just
        tests if the function is executed without error and if all Mlflow functions are called as
        expected.
        """

        self.adapter._client.search_model_versions.return_value = [
            self.dummy_model_version,
            self.dummy_model_version_2,
        ]
        self.adapter.delete_model(model_name="dummy_model")

        # check search_registered_model call
        self.assertEqual(1, self.adapter._client.search_model_versions.call_count)
        self.assertEqual(
            {"filter_string": "name='dummy_model'"},
            self.adapter._client.search_model_versions.call_args.kwargs,
        )

        assert get_artifact_repository_mock.call_count == 2
        assert (
            get_artifact_repository_mock.return_value.delete_artifacts.call_count == 2
        )

        # check delete_registered_model call
        self.assertEqual(1, self.adapter._client.delete_registered_model.call_count)
        self.assertEqual(
            {"name": "dummy_model"},
            self.adapter._client.delete_registered_model.call_args.kwargs,
        )

        # check delete_run call
        self.assertEqual(2, self.adapter._client.delete_run.call_count)
        self.assertEqual(
            {"run_id": 42},
            self.adapter._client.delete_run.call_args_list[0].kwargs,
        )
        self.assertEqual(
            {"run_id": 66},
            self.adapter._client.delete_run.call_args_list[1].kwargs,
        )

    def test_delete_model_from_registry_fails(self) -> None:
        """Test if exception is raised, when one of the mlflow calls fails."""

        self.adapter._client.delete_registered_model.side_effect = Exception
        with self.assertRaises(Exception):
            self.adapter.delete_model(model_name="dummy_model")
            self.assertEqual(
                1, self.adapter._client.delete_registered_models.call_count
            )
            self.assertEqual(
                {"name": "dummy_model"},
                self.adapter._client.delete_registered_models.call_args.kwargs,
            )

    def test_get_download_link_for_model(self) -> None:
        """Test if download link is successfully returned."""
        self.adapter._client.get_run.return_value = self.dummy_infos
        self.adapter._s3_adapter.create_downloadable_zip_of_files.return_value = (
            "https://dummy_download_link/dummy_bucket/path/to/dummy_file.zip"
        )

        link = self.adapter.get_download_link_for_model(model_name="unique_dummy_model")

        # check returned download link
        self.assertEqual(
            link, "https://dummy_download_link/dummy_bucket/path/to/dummy_file.zip"
        )

        # check call of s3 connector mock
        self.assertEqual(
            1, self.adapter.s3_adapter.create_downloadable_zip_of_files.call_count
        )
        expected_call_args = {
            "zipfile_name": "unique_dummy_model.zip",
            "bucket": "dummy_bucket",  # same bucket as in dummy_infos
            "key": "path/to/dummy_file.txt",  # same key as in dummy_infos
        }
        self.assertEqual(
            expected_call_args,
            self.adapter.s3_adapter.create_downloadable_zip_of_files.call_args.kwargs,
        )

    def test_log_artifact(self) -> None:
        """Test if the artifact is logged correctly."""
        log_artifact_mock = MagicMock()
        self.adapter._client.log_artifact = log_artifact_mock
        self.adapter.log_artifact("1", "example/path")
        log_artifact_mock.assert_called_once_with(run_id="1", local_path="example/path")

    def test_log_param(self) -> None:
        """Test if a single param is logged."""
        log_param_mock = MagicMock()
        self.adapter._client.log_param = log_param_mock
        self.adapter.log_param("1", key="param1", value="value1")
        self.assertEqual(1, log_param_mock.call_count)
        log_param_mock.assert_called_once_with(run_id="1", key="param1", value="value1")

    def test_log_params(self) -> None:
        """Test if all params are logged correctly."""
        log_param_mock = MagicMock()
        self.adapter._client.log_param = log_param_mock
        test_params = {"param1": "value1", "param2": "value2", "param3": "value3"}
        self.adapter.log_params("1", test_params)
        self.assertEqual(3, log_param_mock.call_count)
        log_param_mock.assert_any_call(run_id="1", key="param1", value="value1")
        log_param_mock.assert_any_call(run_id="1", key="param2", value="value2")
        log_param_mock.assert_any_call(run_id="1", key="param3", value="value3")

    def test_log_image(self) -> None:
        """Test if an image is logged correctly."""
        log_image_mock = MagicMock()
        self.adapter._client.log_image = log_image_mock
        run_id = "1"
        image = cv2.UMat()
        artifact_file = "path/to/image/image.png"
        self.adapter.log_image(run_id, image, artifact_file)
        log_image_mock.assert_called_once_with(
            run_id=run_id, image=image, artifact_file=artifact_file
        )

    def test_log_metric(self) -> None:
        """Test if a single param is logged."""
        log_metric_mock = MagicMock()
        self.adapter._client.log_metric = log_metric_mock
        self.adapter.log_metric("1", key="score", value=0.5, step=1)
        self.assertEqual(1, log_metric_mock.call_count)
        log_metric_mock.assert_called_once_with(
            run_id="1", key="score", value=0.5, step=1
        )

    @mock.patch("mlflow.set_tracking_uri")
    @mock.patch("mlflow.MlflowClient")
    def test_init(self, mock_mlflowclient, mock_set_tracking_uri) -> None:
        # Set up the environment for the test
        for set_credentials in [True, False]:
            with self.subTest(set_credentials=set_credentials):
                connector = MlflowAdapter(self.credentials, set_credentials)

                # Assertions to check if the initialisation is done correctly
                mock_set_tracking_uri.assert_called_with(
                    uri=self.credentials.server_uri
                )
                self.assertIsInstance(connector._client, MlflowClient)
                self.assertEqual(connector.credentials, self.credentials)

    @mock.patch("os.path.join")
    @mock.patch("mlflow.artifacts.download_artifacts")
    @mock.patch("mlflow.get_run")
    def test_download_model_s3(
        self,
        mock_get_run,
        mock_download_artifacts,
        mock_os_path_join,
    ) -> None:
        # Mock the responses to return the expected values
        mock_model_version = MagicMock()
        mock_model_version.latest_versions[0].run_id = "some-run-id"
        mock_model_version.latest_versions[0].source = "some-source"
        real_model_name = "test-model"
        real_model_dir_path = "/path/to/model/dir"
        wrong_model_name = "wrong-model"
        wrong_model_dir_path = "/path/notTo/model/dir"

        expected_artifact_uri_1 = "some-source/model.yaml"

        def mock_search_registered_models_func(filter_string, max_results):
            if filter_string == f"name='{real_model_name}'":
                return [mock_model_version]
            else:
                raise MlflowException("wrong model name")

        def mock_download_artifacts_func(artifact_uri, dst_path):
            if dst_path == real_model_dir_path:
                return
            raise MlflowException("wrong path")

        self.adapter._client.search_registered_models.side_effect = (
            mock_search_registered_models_func
        )
        mock_download_artifacts.side_effect = mock_download_artifacts_func

        mock_run = MagicMock()
        mock_run.data.params = {
            "best_checkpoint": "best-checkpoint-name",
            "model_type_name": "example-model-type",
        }
        mock_get_run.return_value = mock_run

        # Assume the join method returns the path as expected
        mock_os_path_join.side_effect = lambda *args: "/".join(args)

        for model_name in [real_model_name, wrong_model_name]:
            for model_dir_path in [real_model_dir_path, wrong_model_dir_path]:
                with self.subTest(model_name=model_name, model_dir_path=model_dir_path):
                    if (
                        model_name == real_model_name
                        and model_dir_path == real_model_dir_path
                    ):
                        (
                            model_config_path,
                            model_checkpoint_path,
                            model_type_name,
                        ) = self.adapter.download_model_s3(model_name, model_dir_path)

                        # Check if search_registered_models was called correctly
                        self.adapter._client.search_registered_models.assert_called_with(
                            filter_string=f"name='{model_name}'", max_results=1
                        )

                        # Check if get_run was called with the correct run_id
                        mock_get_run.assert_called_with("some-run-id")

                        # Check if download_artifacts was called twice with the correct arguments
                        mock_download_artifacts.assert_any_call(
                            artifact_uri=expected_artifact_uri_1,
                            dst_path=model_dir_path,
                        )

                        # Check if the return values are as expected
                        self.assertEqual(
                            model_config_path, "/path/to/model/dir/model.yaml"
                        )
                        self.assertEqual(
                            model_checkpoint_path,
                            "/path/to/model/dir/best-checkpoint-name",
                        )
                        self.assertEqual(model_type_name, "example-model-type")

                    else:
                        (
                            model_config_path,
                            model_checkpoint_path,
                            model_type_name,
                        ) = self.adapter.download_model_s3(model_name, model_dir_path)
                        self.assertIsNone(model_config_path)
                        self.assertIsNone(model_checkpoint_path)
                        self.assertIsNone(model_type_name)

    @mock.patch("requests.get")
    @mock.patch("os.makedirs")
    @mock.patch("os.path.isfile")
    @mock.patch("os.path.join", side_effect=lambda *args: "/".join(args))
    @mock.patch("builtins.open", new_callable=mock_open)
    def test_download_model(
        self,
        mock_open,
        mock_os_path_join,
        mock_isfile,
        mock_makedirs,
        mock_requests_get,
    ) -> None:
        mock_model_version = MagicMock()
        mock_model_version.latest_versions[0].run_id = "some-run-id"

        mock_response_config = MagicMock()
        mock_response_config.content = b"model config contents"
        mock_response_params = MagicMock()
        mock_response_params.json.return_value = {
            "run": {
                "data": {
                    "params": [
                        {"key": "model_type_name", "value": "some-model-type"},
                        {"key": "best_checkpoint", "value": "best-checkpoint-name"},
                    ]
                }
            }
        }

        mock_response_artifacts = MagicMock()
        mock_response = {"files": [{"path": "model.yaml"}]}
        real_status_code = 200
        wrong_status_code = 666

        def mock_search_registered_models_func(filter_string, max_results):
            if filter_string == f"name='{real_model_name}'":
                return [mock_model_version]
            else:
                raise MlflowException("wrong model name")

        self.adapter._client.search_registered_models.side_effect = (
            mock_search_registered_models_func
        )

        url_download_best_ckp = (
            f"{self.credentials.server_uri}/"
            f"get-artifact?path=best-checkpoint-name&run_uuid={mock_model_version.latest_versions[0].run_id}"
        )
        url_all_artifacts = (
            f"{self.credentials.server_uri}"
            f"/ajax-api/2.0/preview/mlflow/artifacts/list?run_uuid="
            f"{mock_model_version.latest_versions[0].run_id}"
        )
        url_run_info = (
            f"{self.credentials.server_uri}"
            f"/ajax-api/2.0/preview/mlflow/runs/get?run_id="
            f"{mock_model_version.latest_versions[0].run_id}"
        )
        url_download_cfg = (
            f"{self.credentials.server_uri}/"
            f"get-artifact?path=model.yaml&run_uuid={mock_model_version.latest_versions[0].run_id}"
        )

        def mock_requests_get_func(url, allow_redirects=True):
            if url == url_all_artifacts:
                return mock_response_artifacts
            if url == url_download_best_ckp:
                return mock_response_config
            if url == url_run_info:
                return mock_response_params
            if url == url_download_cfg:
                return mock_response_config
            raise Exception

        def mock_requests_get_func_B(url, allow_redirects=True):
            if url == url_all_artifacts:
                return mock_response_artifacts
            if url == url_download_best_ckp:
                raise Exception
            if url == url_run_info:
                return mock_response_params
            if url == url_download_cfg:
                return mock_response_config
            raise Exception

        def mock_requests_get_func_C(url, allow_redirects=True):
            if url == url_all_artifacts:
                return mock_response_artifacts
            if url == url_download_best_ckp:
                return mock_response_config
            if url == url_run_info:
                raise Exception
            if url == url_download_cfg:
                return mock_response_config
            raise Exception

        def mock_requests_get_func_D(url, allow_redirects=True):
            if url == url_all_artifacts:
                return mock_response_artifacts
            if url == url_download_best_ckp:
                return mock_response_config
            if url == url_run_info:
                return mock_response_params
            if url == url_download_cfg:
                raise Exception
            raise Exception

        request_mocks = [
            mock_requests_get_func,
            mock_requests_get_func_B,
            mock_requests_get_func_C,
            mock_requests_get_func_D,
        ]

        mock_requests_get.side_effect = mock_requests_get_func

        real_model_name = "test-model"
        model_dir_path = "/path/to/model/dir"
        wrong_model_name = "wrong-model"

        for model_name in [real_model_name, wrong_model_name]:
            for status_code in [real_status_code, wrong_status_code]:
                for request_mock in request_mocks:
                    with self.subTest(
                        model_name=model_name, model_dir_path=model_dir_path
                    ):
                        mock_response_artifacts.json.return_value = mock_response
                        mock_response_params.status_code = status_code
                        mock_requests_get.side_effect = request_mock
                        if (
                            model_name == real_model_name
                            and status_code == real_status_code
                            and request_mock == mock_requests_get_func
                        ):
                            with mock.patch("builtins.open", mock_open()):
                                (
                                    config_file_path,
                                    checkpoint_file_path,
                                    model_type_name,
                                ) = self.adapter.download_model(
                                    model_name, model_dir_path
                                )

                            # Check if search_registered_models was called correctly
                            self.adapter._client.search_registered_models.assert_called_with(
                                filter_string=f"name='{model_name}'", max_results=1
                            )

                            # Check if HTTP requests were made correctly
                            expected_url_artifacts = (
                                "http://test-uri.com/ajax-api/2.0/preview/mlflow/artifacts/list?"
                                "run_uuid=some-run-id"
                            )

                            expected_url_model_cfg = (
                                "http://test-uri.com/get-artifact?"
                                "path=model.yaml&run_uuid=some-run-id"
                            )

                            expected_url_run_info = (
                                "http://test-uri.com/ajax-api/2.0/preview/mlflow/runs/get?"
                                "run_id=some-run-id"
                            )

                            expected_url_checkpoint = (
                                "http://test-uri.com/get-artifact?"
                                "path=best-checkpoint-name&run_uuid=some-run-id"
                            )

                            mock_requests_get.assert_has_calls(
                                [
                                    unittest.mock.call(expected_url_artifacts),
                                    unittest.mock.call(
                                        expected_url_model_cfg,
                                        allow_redirects=True,
                                    ),
                                    unittest.mock.call(expected_url_run_info),
                                    unittest.mock.call(
                                        expected_url_checkpoint,
                                        allow_redirects=True,
                                    ),
                                ]
                            )

                            # Check if the return values are as expected
                            self.assertEqual(
                                config_file_path, "/path/to/model/dir/model.yaml"
                            )
                            self.assertEqual(
                                checkpoint_file_path,
                                "/path/to/model/dir/best-checkpoint-name",
                            )
                            self.assertEqual(model_type_name, "some-model-type")
                        else:
                            with mock.patch("builtins.open", mock_open()):
                                (
                                    config_file_path,
                                    checkpoint_file_path,
                                    model_type_name,
                                ) = self.adapter.download_model(
                                    model_name, model_dir_path
                                )
                                if status_code == real_status_code:
                                    self.assertIsNone(config_file_path)
                                self.assertIsNone(checkpoint_file_path)
                                self.assertIsNone(model_type_name)

    @patch("mlcvzoo_util.adapters.mlflow.adapter.MlflowClient")
    @patch("mlcvzoo_util.adapters.mlflow.adapter.mlflow")
    @patch("mlcvzoo_util.adapters.mlflow.adapter.os")
    def test_get_mlflow_default_experiment_id(
        self, os_mock, mlflow_mock, mlflow_client_mock
    ) -> None:
        os_mock.environ.get.return_value = "TestExperiment"

        experiment_id = self.adapter.get_mlflow_default_experiment_id()

        assert experiment_id is not None

    @patch("mlcvzoo_util.adapters.mlflow.adapter.MlflowClient")
    @patch("mlcvzoo_util.adapters.mlflow.adapter.mlflow")
    @patch("mlcvzoo_util.adapters.mlflow.adapter.os")
    def test_get_mlflow_default_experiment_id__experiment_not_exist(
        self, os_mock, mlflow_mock, mlflow_client_mock
    ) -> None:
        os_mock.environ.get.return_value = "TestExperiment"

        self.adapter._client.get_experiment_by_name.return_value = None

        experiment_id = self.adapter.get_mlflow_default_experiment_id()

        assert experiment_id is not None

    def test_get_mlflow_default_experiment_id__error(self) -> None:

        with pytest.raises(
            ValueError,
            match="Necessary environment variable 'MLFLOW_DEFAULT_EXPERIMENT' for defining the Mlflow default experiment is not set!",
        ):
            self.adapter.get_mlflow_default_experiment_id()

    @patch(
        "mlcvzoo_util.adapters.mlflow.adapter.MlflowAdapter.get_run_of_registered_model"
    )
    def test_get_model_artifact_location(
        self, get_run_of_registered_model_mock
    ) -> None:

        run_mock = MagicMock()
        run_mock.info.artifact_uri = "s3://model-registry/test_model"

        get_run_of_registered_model_mock.return_value = run_mock

        s3_data_location = self.adapter.get_model_artifact_location(
            model_name="TestModel"
        )

        assert s3_data_location == S3DataLocation(
            credentials=S3Credentials(
                s3_artifact_access_key="test_access_key",
                s3_artifact_access_id="test_access_id",
                s3_endpoint_url="http://test-artifact-uri.com",
                s3_region_name="eu-central-1",
            ),
            uri="s3://model-registry/test_model",
            location_id="",
        )

    def test_get_model_checkpoint_location(self) -> None:

        run_mock = MagicMock()
        run_mock.data.params.get.return_value = "test.pth"
        run_mock.info.artifact_uri = "s3://model-registry/test_model"

        s3_data_location = self.adapter.get_model_checkpoint_location(run=run_mock)

        assert s3_data_location == S3DataLocation(
            credentials=S3Credentials(
                s3_artifact_access_key="test_access_key",
                s3_artifact_access_id="test_access_id",
                s3_endpoint_url="http://test-artifact-uri.com",
                s3_region_name="eu-central-1",
            ),
            uri="s3://model-registry/test_model/test.pth",
            location_id="",
        )

    def test_get_model_config_location(self) -> None:

        run_mock = MagicMock()
        run_mock.info.artifact_uri = "s3://model-registry/test_model"

        s3_data_location = self.adapter.get_model_config_location(run=run_mock)

        assert s3_data_location == S3DataLocation(
            credentials=S3Credentials(
                s3_artifact_access_key="test_access_key",
                s3_artifact_access_id="test_access_id",
                s3_endpoint_url="http://test-artifact-uri.com",
                s3_region_name="eu-central-1",
            ),
            uri="s3://model-registry/test_model/model.yaml",
            location_id="",
        )

    @classmethod
    def tearDownClass(cls) -> None:

        root_dir = get_root_dir(file_path=os.path.realpath(__file__), file_depth=2)
        # delete mlflow logs
        shutil.rmtree(
            path=os.path.join(root_dir, "tests/unit_tests/mlflow_client/mlruns"),
            ignore_errors=True,
        )


@patch("mlcvzoo_util.adapters.mlflow.adapter.MlflowAdapter.log_artifact")
def test_log_model_configuration(
    log_artifact_mock,
    test_config,
    mlflow_adapter,
) -> None:

    mlflow_adapter.log_model_configuration(run_id="12345678", configuration=test_config)

    log_artifact_call_dict = log_artifact_mock.call_args_list[0][1]
    assert log_artifact_call_dict["run_id"] == "12345678"
    assert os.path.basename(log_artifact_call_dict["local_path"]) == "model.yaml"


@patch("mlcvzoo_util.adapters.mlflow.adapter.MlflowAdapter.log_artifact")
def log_model_configuration_dict(
    log_artifact_mock,
    test_config,
    mlflow_adapter,
) -> None:

    mlflow_adapter.log_model_configuration_dict(
        run_id="12345678", configuration=test_config
    )

    log_artifact_call_dict = log_artifact_mock.call_args_list[0][1]
    assert log_artifact_call_dict["run_id"] == "12345678"
    assert os.path.basename(log_artifact_call_dict["local_path"]) == "model.yaml"


if __name__ == "__main__":
    unittest.main()
