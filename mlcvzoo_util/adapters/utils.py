# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Module for adapter related utility functions.
"""

import logging
from typing import Dict

logger = logging.getLogger(__name__)


def update_string_replacement_map(
    string_replacement_map: Dict[str, str], replacement_key: str, replacement_value: str
) -> None:
    if replacement_key not in string_replacement_map:
        logger.info(
            "Update string-replacement-map: key='%s', value='%s'",
            replacement_key,
            replacement_value,
        )
        string_replacement_map[replacement_key] = replacement_value
    else:
        if string_replacement_map[replacement_key] != replacement_value:
            raise ValueError(
                f"Replacement key '{replacement_key} already exists with different value."
            )
