# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Wrapper module defining the S3Adapter for interaction with an S3 storage via boto3.client.
"""

import json
import logging
import os
import tempfile
import zipfile
from typing import Any, Dict, List, Optional, Tuple, Union, cast

import boto3  # type: ignore[import-untyped]
import yaml
from mypy_boto3_s3 import S3Client

from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlcvzoo_util.adapters.s3.data_location import S3Tuple
from mlcvzoo_util.adapters.utils import update_string_replacement_map

logger = logging.getLogger(__name__)


class S3Adapter:
    """
    Adapter for handling data in a S3 storage.
    """

    def __init__(
        self,
        credentials: Union[S3Credentials, Dict[str, Any]],
        signature_version: Optional[str] = None,
    ) -> None:
        if isinstance(credentials, dict):
            _credentials = S3Credentials(**credentials)
        else:
            _credentials = credentials

        self._credentials = _credentials
        self._client: S3Client = self.__init_client(
            s3_credentials=_credentials, signature_version=signature_version
        )

    @property
    def client(self) -> S3Client:
        return self._client

    @property
    def credentials(self) -> S3Credentials:
        """Get the credentials of the S3 storage."""
        return self._credentials

    @staticmethod
    def __init_client(
        s3_credentials: S3Credentials,
        signature_version: Optional[str] = None,
    ) -> S3Client:
        """
        Init the S3 client.
        """

        if signature_version:
            s3_config = boto3.session.Config(signature_version=signature_version)
        else:
            s3_config = None

        return boto3.client(  # type: ignore[no-any-return]
            service_name="s3",
            region_name=s3_credentials.s3_region_name,
            endpoint_url=s3_credentials.s3_endpoint_url,
            aws_access_key_id=s3_credentials.s3_artifact_access_id,
            aws_secret_access_key=s3_credentials.s3_artifact_access_key,
            config=s3_config,
        )

    def list_files(
        self, bucket: str, prefix: str, filter_empty_files: bool = False
    ) -> List[S3Tuple]:
        """List all files under the prefix of a given S3 bucket.

        Args:
            bucket: S3 bucket name.
            prefix: S3 prefix to file location.
            filter_empty_files: Optional flag to remove empty files from returned list.

        Returns:
            files: List of tuples (bucket, key of file). Empty list if no files can
            be found for the given prefix.
        """
        files: List[S3Tuple]
        try:
            contents: List[Any] = self._client.list_objects_v2(
                Bucket=bucket, Prefix=prefix
            )["Contents"]
            # Filter empty files by checking if returned file bytes are empty
            if filter_empty_files:
                files = [
                    S3Tuple(bucket, content["Key"])
                    for content in contents
                    if self.get_file_as_bytes(bucket, content["Key"])
                ]
            else:
                files = [S3Tuple(bucket, content["Key"]) for content in contents]
            return files

        except KeyError:
            logger.debug(
                f"For S3 bucket '{bucket}' not data exists for prefix '{prefix}'."
            )
            return []

    def _list_objects(self, bucket: str, prefix: str) -> List[Dict[str, Any]]:
        """List objects under the given prefix in an S3 bucket.

        Args:
            bucket: Name of the S3 bucket.
            prefix: Prefix to the objects ('directory path').

        Return:
             List(dictionary) where dictionary defines an S3 response object.
        """
        paginator = self._client.get_paginator("list_objects_v2")
        return [
            cast(Dict[str, Any], objects)
            for objects in paginator.paginate(Bucket=bucket, Prefix=prefix)
        ]

    def __download_object(
        self,
        bucket: str,
        objects_content: Dict[str, Any],
        download_dir: str,
    ) -> Tuple[Optional[str], Optional[str]]:
        """
        Download a file from an S3 bucket based on the provided objects content dictionary.

        Args:
            bucket: S3 bucket from which to download the objects.
            objects_content: The dictionary represents an S3 response for requesting a single object on S3 storage.
            download_dir: Where to download the objects.

        Returns:
            Tuple(local_path, rel_s3_path) where
            local_path: Local file path of the S3 file.
            rel_s3_path: Relative path for the file on S3 storage.
        """

        if objects_content["Size"] >= 0:
            file_path = os.path.join(download_dir, objects_content["Key"])
            logger.debug("Download S3 file %s to %s", objects_content["Key"], file_path)
            os.makedirs(name=os.path.dirname(file_path), exist_ok=True)

            try:
                return self.download_file(
                    local_path=file_path,
                    bucket=bucket,
                    key=objects_content["Key"],
                )
            except NotADirectoryError as error:
                logger.debug(error)

        return None, None

    def download_file(
        self,
        bucket: str,
        key: str,
        local_path: str,
    ) -> Union[Tuple[str, str], Tuple[None, None]]:
        """Download file from S3 for a given bucket and key to a local path.

        Args:
            bucket: Name of the S3 bucket.
            key: S3 key of the file to download.
            local_path: Where to download the file.

        Returns:
            Tuple(local_path, rel_s3_path) where
            local_path: Local file path of the S3 file.
            rel_s3_path: Relative path for the file on S3 storage.
        """
        self._client.download_file(Bucket=bucket, Key=key, Filename=local_path)

        if os.path.isfile(local_path):
            rel_s3_path = f"{bucket}/{key}"
            return local_path, rel_s3_path

        return None, None

    def upload_file(self, local_path: str, bucket: str, key: str) -> None:
        """
        Upload a file as object to a bucket.

        Args:
            local_path: Path of the file to upload.
            bucket: The bucket into which the file should be uploaded.
            key: The key into which the file should be uploaded.

        Returns:
            None
        """
        self._client.upload_file(
            Filename=local_path,
            Bucket=bucket,
            Key=key,
        )

    def get_file_as_bytes(self, bucket: str, prefix: str) -> bytes:
        """Get given file as binary.

        Args:
            bucket: Bucket in which the image is saved.
            prefix: Prefix of the image.

        Returns:
            As binary encoded file.
        """
        file_as_byte: bytes = self._client.get_object(Bucket=bucket, Key=prefix)[
            "Body"
        ].read()
        return file_as_byte

    def get_file_hash(self, bucket: str, prefix: str) -> str:
        """Get the unique hash of a file.

        Args:

        Returns:
            The file hash.
        """
        headers = self._client.head_object(Bucket=bucket, Key=prefix)

        # In S3, the etag is the md5 hash of the artifact
        return headers["ResponseMetadata"]["HTTPHeaders"]["etag"]

    def put_file(self, file: Union[bytes, str], bucket: str, prefix: str) -> None:
        """Save a file to an S3 storage.

        IMPORTANT: This should only be used, when upload_file is not possible,
                   according to AWS Documentation:

        https://aws.amazon.com/blogs/developer/uploading-files-to-amazon-s3/

        Args:
            file: File to save. Can be encoded as string or bytes object.
            bucket: Bucket in which the file is saved.
            prefix: Prefix under which the file is saved.
        """
        self._client.put_object(Body=file, Bucket=bucket, Key=prefix)

    def generate_presigned_url(
        self,
        bucket: str,
        prefix: str,
    ) -> str:
        """Generate presigned urls for a given bucket and prefix.

        The generated urls allow access via get_object method, e.g. for downloading a file.

        Args:
            bucket: Bucket where the file is stored.
            prefix: Prefix of the file.

        Returns:
            A list of presigned urls.
        """
        return self._client.generate_presigned_url(
            ClientMethod="get_object",
            Params={
                "Bucket": bucket,
                "Key": prefix,
            },
        )

    def generate_presigned_urls(self, files: List[S3Tuple]) -> List[str]:
        """Generate presigned urls for a given list of files.

        Args:
            files: Files to generate presigned urls for. For each file the
                   bucket and the prefix must be provided.

        Returns:
            A list of presigned urls.
        """
        return [
            self.generate_presigned_url(bucket=bucket, prefix=prefix)
            for bucket, prefix in files
        ]

    def create_downloadable_zip_of_files(
        self,
        zipfile_name: str,
        bucket: str,
        key: str,
    ) -> str:
        """Create a downloadable zip file for a list of files.

        The files are defined by a bucket and a common key shared by all files.
        All files which share this bucket and key are zipped.

        Args:
            zipfile_name: Name of the zip file to create.
            bucket: Name of the S3 bucket the files are stored in.
            key: Key of the files.

        Returns:
            A download link for the created zip file.
        """
        s3_objects = self._client.list_objects_v2(Bucket=bucket, Prefix=key)

        if s3_objects["Contents"] is None:
            raise KeyError("No artifact found!")

        files_to_zip = [
            s3_object["Key"]
            for s3_object in s3_objects["Contents"]
            if os.path.splitext(s3_object["Key"])[1] != ".zip"
        ]
        self.__create_zip_file(
            files=files_to_zip,
            bucket=bucket,
            key=key,
            zipfile_name=zipfile_name,
        )

        download_url = self._client.generate_presigned_url(
            ClientMethod="get_object",
            Params={"Bucket": bucket, "Key": os.path.join(key, zipfile_name)},
        )
        return download_url

    def __create_zip_file(
        self, zipfile_name: str, files: List[str], bucket: str, key: str
    ) -> None:
        """Create a zip file of files that are associated with the given
        s3 bucket and key. Upload the created zip-file to the bucket and
        key plus the given zipfile-name

        Args:
            zipfile_name: Name of the zip file to create.
            files: Files to zip.
            bucket: Bucket in which the files are stored.
            key: Key of the files.
        """
        with tempfile.TemporaryDirectory() as tempdir:
            downloaded_files: List[str] = []
            for file in files:
                # the artifact is directly downloaded under its name into the temporary directory
                destination = os.path.join(tempdir, os.path.basename(file))
                self._client.download_file(
                    Bucket=bucket,
                    Key=file,
                    Filename=destination,
                )
                downloaded_files.append(destination)

            zipfile_path = os.path.join(tempdir, zipfile_name)
            with zipfile.ZipFile(file=zipfile_path, mode="a") as new_zipfile:
                for file in downloaded_files:
                    new_zipfile.write(filename=file, arcname=os.path.basename(file))

                # get a key for the new_zipfile; should use the same prefix as the artifacts
                zipfile_key = os.path.join(key, zipfile_name)

            # important to close new_zipfile before uploading, otherwise it will be corrupt
            self._client.upload_file(
                Filename=zipfile_path, Bucket=bucket, Key=zipfile_key
            )

    def download_objects(
        self,
        bucket: str,
        prefix: str,
        location_id: str,
        string_replacement_map: Dict[str, str],
    ) -> Tuple[Dict[str, str], List[str], List[str]]:
        """
        Download all objects from an S3 location utilizing the given
        location. The download directory is determined by finding
        a replacement value for the location ID.
        When it matches the respective environment variable, the
        value of this environment variable is used, otherwise
        the current working directory is chosen as default value.
        The location-id can be used to state S3 files with files
        that are configured within an AnnotationHandler configuration,
        by sharing the same relative path and the location-id as root.

        An object can be any file, e.g. an image or an annotation file.

        Args:
            bucket: The bucket where the yaml file is stored.
            prefix: The prefix of the yaml file.
            location_id: The location ID that defines the root for the download directory.
            string_replacement_map: Dict with values to be replaced as key
                                    and replacement values as values.

        Returns:
            The updated string_replacement_map and a list of downloaded files.
        """
        objects_list = self._list_objects(bucket=bucket, prefix=prefix)
        location_dir = os.getenv(location_id, os.getcwd())
        download_dir = os.path.join(
            location_dir,
            bucket,
        )
        if location_id != "":
            update_string_replacement_map(
                string_replacement_map=string_replacement_map,
                replacement_key=location_id,
                replacement_value=location_dir,
            )

        logger.info("Download and store data from S3 to directory='%s'" % download_dir)

        downloaded_files: List[str] = []
        original_files: List[str] = []
        for objects in objects_list:
            objects_contents = objects.get("Contents")
            if not objects_contents:
                continue

            for objects_content in objects_contents:
                downloaded_file_path, original_file_path = self.__download_object(
                    objects_content=objects_content,
                    download_dir=download_dir,
                    bucket=bucket,
                )

                if downloaded_file_path is not None and original_file_path is not None:
                    downloaded_files.append(downloaded_file_path)
                    original_files.append(original_file_path)

        if not downloaded_files:
            logger.warning(
                f"Did not find any files in bucket='{bucket}' "
                f"with prefix='{prefix}'"
            )

        return string_replacement_map, downloaded_files, original_files

    def get_yaml_file_as_json_string(self, bucket: str, prefix: str) -> str:
        """Get a yaml file as a JSON formatted string.

        Args:
            bucket: The bucket where the yaml file is stored.
            prefix: The prefix of the yaml file.

        Returns:
            The yaml as a JSON formatted string.
        """
        return json.dumps(self.get_yaml_file_as_dict(bucket=bucket, prefix=prefix))

    def get_yaml_file_as_dict(self, bucket: str, prefix: str) -> Dict[str, Any]:
        """Get a yaml file as a dict.

        Args:
            bucket: The bucket where the yaml file is stored.
            prefix: The prefix of the yaml file.

        Returns:
            The yaml as a  dict.
        """
        # check that file is a yaml
        _, file_name = os.path.split(prefix)
        file_extension = file_name.split(".")[-1]
        if file_extension not in ["yml", "yaml"]:
            raise RuntimeError(f"File '{file_name}' is not a YAML file!")

        yaml_dict: Dict[str, Any] = yaml.load(
            stream=self._client.get_object(Bucket=bucket, Key=prefix)["Body"],
            Loader=yaml.Loader,
        )

        return yaml_dict
