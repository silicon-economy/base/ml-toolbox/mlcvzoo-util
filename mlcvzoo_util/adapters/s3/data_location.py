# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Defining a class for accessing S3 files via a given URI and credentials.
"""

from typing import Any, Dict, NamedTuple, Tuple, Union
from urllib.parse import urlparse

import related
from attr import asdict, define

from mlcvzoo_util.adapters.data_location import DataLocation
from mlcvzoo_util.adapters.s3.credentials import S3Credentials


class S3Tuple(NamedTuple):
    """Simple class to store relevant S3 data for data exchange."""

    bucket: str
    prefix: str


@define(init=False)
class S3Data:
    """Parse relevant S3 data from a given URI into a concrete dataclass.

    Attributes:
        uri: Full URI to an S3 object.
    """

    uri: str
    __endpoint: str
    __bucket: str
    __prefix: str

    def __init__(
        self,
        uri: str,
    ):
        """Initialize an S3Data object.

        Args:
             uri: Full URI to an S3 object.

        Raises:
            ValueError: The given URI cannot be parsed, e.g. if an invalid URI
                        is given (no key, wrong scheme etc.).
        """
        self.uri: str = uri
        self.__endpoint, self.__bucket, self.__prefix = self._init_from_uri(uri=uri)

    def to_dict(self) -> Dict[str, str]:
        """
        Returns:
            A dict with the data's URI.
        """
        return {
            "uri": self.uri,
        }

    @property
    def s3_tuple(self) -> S3Tuple:
        """
        Returns:
            An S3Tuple with bucket and prefix information for the data.
        """
        return S3Tuple(bucket=self.bucket, prefix=self.prefix)

    @property
    def endpoint(self) -> str:
        """
        Returns:
            The endpoint part of the S3 URI attribute.
        """
        return self.__endpoint

    @property
    def bucket(self) -> str:
        """
        Returns:
            The bucket part of the S3 URI attribute.
        """
        return self.__bucket

    @property
    def prefix(self) -> str:
        """
        Returns:
            The prefix part of the s3 URI attribute.
        """
        return self.__prefix

    @staticmethod
    def _init_from_uri(uri: str) -> Tuple[str, str, str]:
        """Parse an S3 URI into its parts.

        See structure of S3 URIs as documented in
        https://community.aws/content/2biM1C0TkMkvJ2BLICiff8MKXS9/format-and-parse-amazon-s3-url?lang=en

        Args:
            uri: The URI to parse.

        Returns:
            The URI parsed into endpoint, bucket and prefix.
        """

        s3_global_identifier = "s3://"
        if uri.startswith(s3_global_identifier):
            endpoint = ""
            uri_split = uri.replace(s3_global_identifier, "").split("/")
            bucket_name = uri_split.pop(0)
            prefix = "/".join(uri_split)

            return endpoint, bucket_name, prefix
        else:
            url = urlparse(uri)
            endpoint = f"{url.scheme}://{url.netloc}"
            try:
                # Split by '/' starts with the empty string so first token is in [1]
                bucket_name, prefix = url.path.split("/", 2)[1:]

                return endpoint, bucket_name, prefix

            except (IndexError, ValueError):
                raise ValueError(
                    f"Could not build fully built S3Data object from the given uri='{uri}'."
                )


@define(init=False)
class S3DataLocation(DataLocation):
    """An S3DataLocation object is used to exchange data between
    different services in the ML-Toolbox. It contains all the
    information that is necessary to access a dedicated data object
    in the s3.

    The location id attribute is used to determine shared locations
    between an S3DataLocation and other MLCVZoo configuration data.
    It represents the name of an environment variable and is then
    used as root directory to build absolute file paths in the following
    manner:

    path = os.path.join(os.environ[location_id], + relative-path)

    This ensures that the data of the configurations match to the
    directories where the S3 data is downloaded to.

    Attributes:
        uri: Full URI to a S3 data object.
        credentials: Credentials to access the s3 object at the given uri.
        location_id: The location id that should be used during the download
                     of a S3DataLocation object.
    """

    __related_strict__ = True

    __s3_data: S3Data

    credentials: S3Credentials = related.ChildField(cls=S3Credentials)

    # DataLocation attributes - override due to mro
    uri: str = related.StringField()
    location_id: str = related.StringField(default="")

    def __init__(
        self,
        uri: str,
        credentials: Union[S3Credentials, Dict[str, Any]],
        location_id: str = "",
    ):
        """Initialize a S3 data object.

        Args:
            uri: Full URI to a s3 data object.
            credentials: Credentials to access the s3 object at the given uri.
            location_id: The location id that should be used during the download
                         of a S3DataLocation object.

        Raises:
            ValueError: The given URI cannot be parsed, e.g. if an invalid uri
                        is given (no key, wrong scheme etc.).
        """
        self.uri = uri
        self.__s3_data = S3Data(uri=self.uri)

        if isinstance(credentials, dict):
            self.credentials = S3Credentials(**credentials)
        else:
            self.credentials = credentials

        self.location_id = location_id

    def to_dict(self) -> Dict[str, str]:
        return {
            "uri": self.uri,
            "location_id": self.location_id,
            "credentials": asdict(self.credentials),  # type: ignore[dict-item]
        }

    @property
    def s3_tuple(self) -> S3Tuple:
        return S3Tuple(bucket=self.bucket, prefix=self.prefix)

    @property
    def endpoint(self) -> str:
        """
        Returns:
            The endpoint part of the s3 URI attribute.
        """
        return self.__s3_data.endpoint

    @property
    def bucket(self) -> str:
        """
        Returns:
            The bucket part of the s3 URI attribute.
        """
        return self.__s3_data.bucket

    @property
    def prefix(self) -> str:
        """
        Returns:
            The prefix part of the s3 URI attribute.
        """
        return self.__s3_data.prefix
