# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining dataclasses for different credentials."""

from __future__ import annotations

import os
from enum import Enum
from typing import Dict

import related
from attr import asdict, define

__error_message__: str = (
    "Necessary environment variable '{}' " "for accessing {} has not been set!"
)


class S3CredentialConstants(Enum):
    """Defines S3 credential constants."""

    S3_ACCESS_KEY_ID = "S3_ACCESS_KEY_ID"

    S3_SECRET_ACCESS_KEY = "S3_SECRET_ACCESS_KEY"

    S3_ENDPOINT = "S3_ENDPOINT"

    S3_REGION_NAME = "S3_REGION_NAME"


@define
class S3Credentials:
    """Defines S3 credentials used to access S3 storages.

    Attributes:
        s3_endpoint_url: URL endpoint of the S3 storage.
        s3_region_name: Region of the s3 storage.
        s3_artifact_access_key: Secret access key for a S3 storage.
        s3_artifact_access_id: Access ID for a S3 storage.
    """

    # Hide keys from repr to not leak them during logging
    s3_endpoint_url: str = related.StringField()
    s3_region_name: str = related.StringField()
    s3_artifact_access_key: str = related.StringField(repr=False)
    s3_artifact_access_id: str = related.StringField(repr=False)

    def to_dict(self) -> Dict[str, str]:
        return asdict(self)

    @staticmethod
    def init_from_os_environment() -> S3Credentials:
        """Inits a S3Credentials object from the environment variables.

        Returns:
            The created S3Credentials.
        """
        module_error_string = "the S3 storage"

        def get_credential_constant(const: str) -> str:
            """Return value for a credential constant from the os environment variables.

            Args:
                const: Environment variable key for the constant.

            Raises:
                ValueError: If no value for a credential constant can be found in the environment variables.

            Returns:
                The value for a given constant key in the environment variables.
            """
            value = os.getenv(const)
            if value is None:
                raise ValueError(__error_message__.format(const, module_error_string))
            return value

        s3_artifact_access_key = get_credential_constant(
            S3CredentialConstants.S3_SECRET_ACCESS_KEY.value
        )
        s3_artifact_access_id = get_credential_constant(
            S3CredentialConstants.S3_ACCESS_KEY_ID.value
        )
        s3_endpoint_url = get_credential_constant(
            S3CredentialConstants.S3_ENDPOINT.value
        )
        s3_region_name = get_credential_constant(
            S3CredentialConstants.S3_REGION_NAME.value
        )

        return S3Credentials(
            s3_artifact_access_key=s3_artifact_access_key,
            s3_artifact_access_id=s3_artifact_access_id,
            s3_endpoint_url=s3_endpoint_url,
            s3_region_name=s3_region_name,
        )
