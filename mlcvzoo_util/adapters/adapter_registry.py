# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

from typing import Any, Dict

from mlcvzoo_util.adapters.constants import (
    ImplementedMetricAdapters,
    ImplementedStorageAdapters,
)
from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.s3.data_location import S3DataLocation

adapter_registry: Dict[str, Any] = {
    ImplementedStorageAdapters.S3.value: S3DataLocation,
    ImplementedMetricAdapters.MLFLOW.value: MlflowCredentials,
}
