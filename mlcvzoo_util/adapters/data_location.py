# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Providing a class for accessing files via a given uri and credentials
"""

from attr import define


@define
class DataLocation:
    """Base data item with its associated location.

    Data items can be images, video files etc. with their associated locations,
    e.g. file path, link, url etc.

    The location_id will be used to identify the directory where the data will be stored.
    It will be used to match an environment variable name that stores the path to the
    respective directory.

    Attributes:
        uri: Unique reference to the location of a data item.
        location_id: Location ID of a DataLocation
    """

    uri: str
    location_id: str = ""
