# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining a client to connect to a Mlflow server."""

import ast
import logging
import os
import tempfile
from datetime import datetime
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

import mlflow
import requests
import yaml
from config_builder import BaseConfigClass
from config_builder.json_encoding import TupleEncoder
from mlcvzoo_base.api.data.types import ImageType
from mlcvzoo_base.api.interfaces import NetBased
from mlcvzoo_base.api.model import Model
from mlcvzoo_base.configuration.model_config import ModelConfig
from mlcvzoo_base.models.model_registry import ModelRegistry
from mlflow.entities.run import Run
from mlflow.exceptions import MlflowException, RestException
from mlflow.store.artifact.artifact_repository_registry import get_artifact_repository

from mlcvzoo_util.adapters.mlflow.credentials import MlflowCredentials
from mlcvzoo_util.adapters.mlflow.structs import (
    ModelInfo,
    RegisteredModelLoggedParams,
    TrainingStatus,
)
from mlcvzoo_util.adapters.s3.adapter import S3Adapter
from mlcvzoo_util.adapters.s3.credentials import S3Credentials
from mlcvzoo_util.adapters.s3.data_location import S3Data, S3DataLocation

logger = logging.getLogger(__name__)

try:
    from mlflow import MlflowClient
except ImportError as import_error:
    logger.error(
        "ImportError: %s, will try to use 'mlflow.tracking import MlflowClient' instead"
        % (str(import_error))
    )
    from mlflow.tracking import MlflowClient


class MlflowAdapter:
    """Adapter to an MLflow server.

    Attributes:
        credentials: Credentials to connect to the server.
    """

    __MODEL_CONFIG_FILE_NAME__ = "model.yaml"
    __MLFLOW_DEFAULT_EXPERIMENT_NAME__ = "MLFLOW_DEFAULT_EXPERIMENT"

    def __init__(
        self,
        credentials: MlflowCredentials,
        set_credentials: bool = False,
    ):
        """Init an MlflowAdapter.

        Args:
            credentials: Credentials to access the MlFlow server.
            set_credentials: Defines if credentials are set as environment variables. Credentials
                            must be set as environment variables for the adapter to work.
        """
        self.credentials: MlflowCredentials = credentials

        if set_credentials:
            self.credentials.update_os_environment()

        self._s3_adapter: S3Adapter = S3Adapter(
            credentials=S3Credentials(
                s3_endpoint_url=self.credentials.s3_endpoint_url,
                s3_region_name=self.credentials.s3_region_name,
                s3_artifact_access_key=self.credentials.s3_artifact_access_key,
                s3_artifact_access_id=self.credentials.s3_artifact_access_id,
            )
        )

        # Tracking uri has to be set before initializing the client
        mlflow.set_tracking_uri(uri=self.credentials.server_uri)
        self._client = MlflowClient()

    @property
    def s3_adapter(self) -> S3Adapter:
        """Get the S3StorageAdapter."""
        return self._s3_adapter

    @property
    def client(self) -> MlflowClient:
        """Get the MlflowClient."""
        return self._client

    def get_model_infos_from_registry(
        self, filter_query: Union[str, None] = None
    ) -> List[ModelInfo]:
        """Get information for all models registered in the MLflow ModelRegistry.

        Args:
            filter_query: Optional filter options to filter the model-infos from registry.

        Returns:
            List of MLflowModelInfo where each element contains information for a registered model.
        """
        model_infos = []

        for registered_model in self._client.search_registered_models(
            filter_string=filter_query
        ):

            if registered_model.latest_versions:
                associated_run = self._client.get_run(
                    run_id=registered_model.latest_versions[0].run_id
                )

                description: str = associated_run.data.params.get(
                    RegisteredModelLoggedParams.DESCRIPTION.value, ""
                )
                baseline_model: str = associated_run.data.params.get(
                    RegisteredModelLoggedParams.BASELINE_MODEL.value, ""
                )
                algorithm_type: str = associated_run.data.params.get(
                    RegisteredModelLoggedParams.ALGORITHM_TYPE.value, ""
                )
                model_type_name: str = associated_run.data.params.get(
                    RegisteredModelLoggedParams.MODEL_TYPE_NAME.value, ""
                )
                epochs: int = associated_run.data.params.get(
                    RegisteredModelLoggedParams.EPOCHS.value, 0
                )
                batch_size: int = associated_run.data.params.get(
                    RegisteredModelLoggedParams.BATCH_SIZE.value, 0
                )
                score: float = associated_run.data.metrics.get(
                    RegisteredModelLoggedParams.SCORE.value, 0.0
                )
                # Some older registered models have single datasets
                # logged under the key "dataset"
                dataset = associated_run.data.params.get("dataset", None)
                if dataset:
                    datasets = [dataset]
                else:
                    _datasets = associated_run.data.params.get(
                        RegisteredModelLoggedParams.DATASETS.value, None
                    )

                    if _datasets:
                        datasets = ast.literal_eval(node_or_string=_datasets)
                    else:
                        datasets = [""]

                _evaluation_datasets = associated_run.data.params.get(
                    RegisteredModelLoggedParams.EVALUATION_DATASETS.value, None
                )
                if _evaluation_datasets:
                    evaluation_datasets = ast.literal_eval(
                        node_or_string=_evaluation_datasets
                    )
                else:
                    evaluation_datasets = [""]

                # Get state from individually logged params in MLflow,
                # the order defines the correct state
                if TrainingStatus.FINISHED.value in associated_run.data.params:
                    state = TrainingStatus.FINISHED.value
                elif TrainingStatus.CRASHED.value in associated_run.data.params:
                    state = TrainingStatus.CRASHED.value
                elif TrainingStatus.RUNNING.value in associated_run.data.params:
                    state = TrainingStatus.RUNNING.value
                elif TrainingStatus.NEW.value in associated_run.data.params:
                    state = TrainingStatus.NEW.value
                else:
                    state = ""

                mlflow_url: str = (
                    f"{self.credentials.server_uri}/#/models/{registered_model.name}"
                )

                model_info = ModelInfo(
                    run_id=associated_run.info.run_id,
                    name=registered_model.name,
                    training_state=state,
                    # datetime from timestamp given in milliseconds since UNIX epoch
                    creation_date=datetime.fromtimestamp(
                        registered_model.last_updated_timestamp / 1000.0
                    ).isoformat(),
                    baseline_model=baseline_model,
                    algorithm_type=algorithm_type,
                    model_type_name=model_type_name,
                    epochs=epochs,
                    batch_size=batch_size,
                    datasets=datasets,
                    score=score,
                    description=description,
                    evaluation_datasets=evaluation_datasets,
                    mlflow_url=mlflow_url,
                )

                model_infos.append(model_info)

        return model_infos

    def delete_model(self, model_name: str) -> None:
        """Delete a model from the model registry and its associated training run.

        If the model or the run do not exist, an exception is raised.

        Args:
            model_name: Unique name of model to delete.

        Raises:
            RestException: Model or associated run not found.
        """
        associated_runs = [
            model_version.run_id
            for model_version in self._client.search_model_versions(
                filter_string=f"name='{model_name}'"
            )
        ]

        try:
            for run_id in associated_runs:
                run = self.client.get_run(run_id=run_id)

                repository = get_artifact_repository(run.info.artifact_uri)
                repository.delete_artifacts()  # type: ignore[no-untyped-call]

                self._client.delete_run(run_id=run_id)

            self._client.delete_registered_model(name=model_name)

        except RestException as exception:
            raise exception

    def get_mlflow_default_experiment_id(self) -> str:
        """Get the ID of the Mlflow default experiment.

        Returns:
            The ID of the default experiment.
        """
        experiment_name = os.environ.get(self.__MLFLOW_DEFAULT_EXPERIMENT_NAME__, None)

        if experiment_name is None:
            raise ValueError(
                f"Necessary environment variable '{self.__MLFLOW_DEFAULT_EXPERIMENT_NAME__}' "
                "for defining the Mlflow default experiment is not set!"
            )

        experiment = self._client.get_experiment_by_name(name=experiment_name)

        experiment_id: str
        # if exp not created yet, create it, otherwise just get the id
        if experiment is None:
            experiment_id = mlflow.create_experiment(name=experiment_name)
        else:
            experiment_id = experiment.experiment_id

        return experiment_id

    def get_download_link_for_model(
        self,
        model_name: str,
    ) -> str:
        """Get download link for a model and its associated artifacts.

        Args:
            model_name: Name of the model to download.

        Returns:
            Download link for the model and its associated artifacts.
        """
        # get the required information from the model registry
        associated_run = self.get_run_of_registered_model(model_name=model_name)
        s3_data = S3Data(uri=associated_run.info.artifact_uri)

        return self._s3_adapter.create_downloadable_zip_of_files(
            zipfile_name=f"{model_name}.zip", bucket=s3_data.bucket, key=s3_data.prefix
        )

    def get_run_of_registered_model(self, model_name: str) -> Run:
        """Get the associated run of a registered model.

        Args:
            model_name: Name of the registered model.

        Returns:
            Associated run.
        """
        registered_model_info = self._client.search_registered_models(
            filter_string=f"name='{model_name}'"
        )
        associated_run = self._client.get_run(
            run_id=registered_model_info[0].latest_versions[0].run_id
        )
        return associated_run

    def create_run_id(self) -> str:
        """Create a new MLflow run.

        Returns:
              The run id of the new Mlflow run.
        """
        return str(
            self._client.create_run(
                experiment_id=self.get_mlflow_default_experiment_id()
            ).info.run_id
        )

    def log_param(self, run_id: str, key: Any, value: Any) -> None:
        """Log a single parameter (key-value-pair) fo MLflow.

        Args:
            run_id: ID of the run being logged.
            key: Key for the logged param.
            value: Value for the logged param.
        """
        self._client.log_param(run_id=run_id, key=key, value=value)

    def log_params(self, run_id: str, params: Dict[str, Any]) -> None:
        """Log the provided parameters to Mlflow.

        Args:
            run_id: ID of the run being logged.
            params: The params to log.
        """
        for param, value in params.items():
            self._client.log_param(run_id=run_id, key=param, value=value)

    def log_artifact(self, run_id: str, local_path: str) -> None:
        """Log an artifact to Mlflow.

        Args:
            run_id: ID of the run being logged.
            local_path: Path to the local file.
        """
        self._client.log_artifact(run_id=run_id, local_path=local_path)

    def log_image(self, run_id: str, image: ImageType, artifact_file: str) -> None:
        """Log an image to Mlflow.

        Args:
            run_id: ID of the run being logged.
            image: A cv2.Umat object representing an image file.
            artifact_file: Path to the logged artifact file.
        """
        self._client.log_image(run_id=run_id, image=image, artifact_file=artifact_file)

    def log_metric(
        self,
        run_id: str,
        key: str,
        value: float,
        step: Optional[int] = None,
    ) -> None:
        """Log a score for a mlflow run.

        Args:
            run_id: ID of the run being logged.
            key: Metric name.
            value: Metric value.
            step: Step of the metric.
        """
        self._client.log_metric(run_id=run_id, key=key, value=value, step=step)

    def get_class_metrics(self, model_name: str) -> Dict[str, Dict[str, str]]:
        """Retrieve metrics for all classes of the specified registered model.

        Args:
            model_name: Name of the registered model.

        Returns:
            A dictionary containing metrics for all classes,
            with class names as keys and their respective metrics as inner dictionaries.
        """
        class_metrics: Dict[str, Dict[str, str]] = {}

        associated_run = self.get_run_of_registered_model(model_name)
        metrics = associated_run.data.metrics

        best_checkpoint_step = associated_run.data.params.get(
            RegisteredModelLoggedParams.BEST_CHECKPOINT_STEP.value, None
        )
        if best_checkpoint_step is None:
            return class_metrics

        best_checkpoint_step = int(best_checkpoint_step)

        for metric_name in metrics.keys():
            if not metric_name.endswith("_ALL"):
                continue

            class_name = self.__extract_class_name(metric_name)

            if class_name not in class_metrics:
                class_metrics[class_name] = {}

            metric_name_splitted = self.__extract_metric_name(metric_name)

            metric_history = self._client.get_metric_history(
                associated_run.info.run_id, metric_name
            )
            metric_value: Optional[float] = None
            for metric_entry in metric_history:
                if metric_entry.step == best_checkpoint_step:
                    metric_value = metric_entry.value

            if metric_value is not None:
                metric_value_str = self.__truncate_metric_value(metric_value)
            else:
                metric_value_str = "-"

            class_metrics[class_name][metric_name_splitted] = metric_value_str

        return class_metrics

    @staticmethod
    def register_mlflow_model(run_id: str, unique_name: str) -> None:
        """Register the model in Mlflow.

        If the model does not exist, also creates new model.

        Args:
            run_id: ID of the Mlflow run.
            unique_name: Name of the model.
        """
        # Register Model before starting the training
        model_uri = f"runs:/{run_id}"

        mlflow.register_model(
            model_uri=model_uri,
            name=unique_name,
        )

    @staticmethod
    def __extract_class_name(metric_name: str) -> str:
        """Extract class name from the given metric name.

        A metric name has the structure of the class ID, the class name and
        the metric ALL separated by an underscore e.g.: 0_custom_class_FN_ALL.

        Args:
            metric_name: The metric name from which class name needs to be extracted.

        Returns:
            The extracted class name.
        """
        class_name_parts = metric_name.split("_")
        return "_".join(class_name_parts[:-2])

    @staticmethod
    def __extract_metric_name(metric_name: str) -> str:
        """Extract metric name for the class from the given metric name.

        A metric name has the structure of the class ID, the class name and
        the metric ALL separated by an underscore e.g.: 0_custom_class_FN_ALL

        Args:
            metric_name: The metric name from which class name needs to be extracted.

        Returns:
            The extracted metric name for the class.
        """
        return metric_name.rsplit("_", 1)[0].rsplit("_", 1)[1]

    @staticmethod
    def __truncate_metric_value(metric_value: float) -> str:
        """Convert and truncate metric value to two decimal places.

        Args:
            metric_value: The metric value to be converted and truncated.

        Returns:
            The converted and truncated metric value.
        """
        return str(round(metric_value, 4))

    def get_model_artifact_location(
        self, model_name: str, location_id: str = ""
    ) -> S3DataLocation:
        """Get the S3DataLocation object representing the information
        about the root of model artifacts.

        Args:
            model_name: Mlflow run which is associated to the model config.
            location_id: location ID to use for the creation of the S3DataLocation.

        Returns:
            S3DataLocation object specifying the model config location.
        """
        # The artifacts for all models are always stored under the same name
        run = self.get_run_of_registered_model(model_name=model_name)

        return S3DataLocation(
            uri=run.info.artifact_uri,
            location_id=location_id,
            credentials=self.credentials.to_s3_credentials(),
        )

    def get_model_checkpoint_location(
        self, run: Run, location_id: str = ""
    ) -> S3DataLocation:
        """Get the storage location of the model.

        Args:
            run: Mlflow run which is associated to the model.
            location_id: location ID to use for the creation of the S3DataLocation.

        Returns:
            S3DataLocation object specifying the model location.

        Raises:
            KeyError if no best checkpoint is logged.
        """
        best_checkpoint: Optional[str] = run.data.params.get(
            RegisteredModelLoggedParams.BEST_CHECKPOINT.value, None
        )
        if not best_checkpoint:
            raise KeyError(
                "There is no best checkpoint logged! Check the registered model in "
                "Mlflow or use a different model for deployment!"
            )

        return S3DataLocation(
            uri=os.path.join(run.info.artifact_uri, best_checkpoint),
            location_id=location_id,
            credentials=self.credentials.to_s3_credentials(),
        )

    def get_model_config_location(
        self, run: Run, location_id: str = ""
    ) -> S3DataLocation:
        """Get the storage location of a model config.
        The filename of the model config is the same for all models.

        Args:
            run: Mlflow run which is associated to the model config.
            location_id: location ID to use for the creation of the S3DataLocation.

        Returns:
            S3DataLocation object specifying the model config location.
        """

        return S3DataLocation(
            uri=os.path.join(
                run.info.artifact_uri,
                MlflowAdapter.__MODEL_CONFIG_FILE_NAME__,
            ),
            location_id=location_id,
            credentials=self.credentials.to_s3_credentials(),
        )

    def get_model_config_dict(
        self,
        model_name: str,
    ) -> Dict[str, Any]:
        """
        Downloads the model configuration as dict for the given model-name
        utilizing the MLFlow-Adapter.

        Args:
            model_name: The model-name for which to download the dictionary

        Returns:
            The model configuration as dictionary
        """
        return self.get_model_config_dict_from_run(
            run=self.get_run_of_registered_model(model_name),
        )

    def get_model_config_dict_from_run(self, run: Run) -> Dict[str, Any]:
        """
        Download the model configuration as dictionary that is
        associated with the given MLflow run.

        Args:
            run: The MLflow run from which to download the model configuration

        Returns:
            The model configuration as dictionary
        """
        location = self.get_model_config_location(run=run)

        return self._s3_adapter.get_yaml_file_as_dict(
            bucket=location.bucket, prefix=location.prefix
        )

    def init_model(
        self,
        model_name: str,
        model_dir_path: str,
        registry: Optional[ModelRegistry] = None,  # type: ignore[type-arg]
        string_replacement_map: Optional[Dict[str, str]] = None,
        constructor_parameters: Optional[
            Dict[
                str,
                Union[
                    int, bool, str, float, List[Any], Dict[str, Any], BaseConfigClass
                ],
            ]
        ] = None,
        init_for_inference: bool = True,
    ) -> Optional[Model]:  # type: ignore[type-arg]
        """Initialize a model from a model registry.

        Args:
            model_name: Unique model identifier in registry.
            model_dir_path: S3 key to the model ("directory" of the model).
            registry: Model registry from which to init the model. If None is provided, a ModelRegistry with default
                      arguments is initialized.
            string_replacement_map: Dict with values to be replaced as key and replacement values as values.
            constructor_parameters: Additional constructor params to be passed to ModelConfig.
            init_for_inference: Flag to set model in inference mode.

        Returns:
            The loaded model or None if no model could be loaded from the registry.

        """
        (
            config_file_path,
            checkpoint_file_path,
            model_type_name,
        ) = self.download_model_s3(
            model_name=model_name,
            model_dir_path=model_dir_path,
        )

        model: Optional[Model] = None  # type: ignore[type-arg]
        if config_file_path and model_type_name:
            if registry is None:
                registry = ModelRegistry()

            if constructor_parameters is None:
                constructor_parameters = {
                    "from_yaml": config_file_path,
                    "init_for_inference": init_for_inference,
                }

            model_config: ModelConfig = ModelConfig(
                class_type=model_type_name,
                constructor_parameters=constructor_parameters,
            )

            model = registry.init_model(
                model_config=model_config,
                string_replacement_map=string_replacement_map,
            )

        if model and isinstance(model, NetBased) and checkpoint_file_path:
            model.restore(checkpoint_path=checkpoint_file_path)

        return model

    def download_model_s3(
        self, model_name: str, model_dir_path: str
    ) -> Tuple[Optional[str], Optional[str], Optional[str]]:
        """Download a model with S3 credentials.

        Args:
            model_name: Name of the model.
            model_dir_path: Destination path.

        Returns:
            A Tuple(config_file_path, checkpoint_file_path, model_type_name) where

            config_file_path: The local yaml configuration filepath of the model.
            checkpoint_file_path: The local checkpoint filepath of the model.
            model_type_name: The type of the model from the MLCVZooRegistry.
        """
        try:
            model = self._client.search_registered_models(
                filter_string=f"name='{model_name}'", max_results=1
            )
        except MlflowException as e:
            logger.error(e)
            return None, None, None

        best_mlflow_run = mlflow.get_run(model[0].latest_versions[0].run_id)

        best_checkpoint_name = best_mlflow_run.data.params[
            RegisteredModelLoggedParams.BEST_CHECKPOINT.value
        ]
        model_type_name = best_mlflow_run.data.params[
            RegisteredModelLoggedParams.MODEL_TYPE_NAME.value
        ]
        artifacts_prefix = model[0].latest_versions[0].source

        try:
            mlflow.artifacts.download_artifacts(
                artifact_uri=f"{artifacts_prefix}/{best_checkpoint_name}",
                dst_path=model_dir_path,
            )
            mlflow.artifacts.download_artifacts(
                artifact_uri=f"{artifacts_prefix}/{self.__MODEL_CONFIG_FILE_NAME__}",
                dst_path=model_dir_path,
            )
        except MlflowException as e:
            logger.error(e)
            return None, None, None

        return (
            os.path.join(model_dir_path, self.__MODEL_CONFIG_FILE_NAME__),
            os.path.join(model_dir_path, best_checkpoint_name),
            model_type_name,
        )

    def download_model(
        self, model_name: str, model_dir_path: str
    ) -> Tuple[Optional[str], Optional[str], Optional[str]]:
        """Download model without S3 credentials.

        Args:
            model_name: Name of model.
            model_dir_path: Destination path.

        Returns:
            A Tuple(config_file_path, checkpoint_file_path, model_type_name) where

            config_file_path: The local yaml configuration filepath of the model.
            checkpoint_file_path: The local checkpoint filepath of the model.
            model_type_name: The type of the model from the MLCVZooRegistry.
        """
        os.makedirs(model_dir_path, exist_ok=True)

        try:
            model = self._client.search_registered_models(
                filter_string=f"name='{model_name}'", max_results=1
            )
        except MlflowException as e:
            logger.error(e)
            return None, None, None

        run_id = getattr(model[0].latest_versions[0], "run_id", None)
        url_all_artifacts = (
            f"{self.credentials.server_uri}/"
            f"ajax-api/2.0/preview/mlflow/artifacts/list?run_uuid={run_id}"
        )
        url_run_info = (
            f"{self.credentials.server_uri}/"
            f"ajax-api/2.0/preview/mlflow/runs/get?run_id={run_id}"
        )

        # get all artifacts
        try:
            response_artifacts = requests.get(url_all_artifacts)
        except requests.exceptions.RequestException as e:
            logger.error(e)
            return None, None, None

        # Download .yaml config
        config_file_path = None
        for filename in response_artifacts.json()["files"]:
            if ".yaml" in filename["path"]:
                url_download_cfg = (
                    f"{self.credentials.server_uri}/"
                    f"get-artifact?path={filename['path']}&run_uuid={run_id}"
                )
                config_file_path = os.path.join(model_dir_path, filename["path"])
                try:
                    logger.info(f"Start download: {url_download_cfg} ... ")
                    response_download_config = requests.get(
                        url_download_cfg, allow_redirects=True
                    )
                    with open(config_file_path, "wb") as f:
                        f.write(response_download_config.content)
                        logger.info(f"Download finished: {config_file_path}")
                except Exception as e:
                    logger.error(e)
                    return None, None, None

        # Download best checkpoint
        checkpoint_file_path: Optional[str] = None
        model_type_name: Optional[str] = None
        try:
            response_params = requests.get(url_run_info)
            if response_params.status_code == 200:
                params = response_params.json()["run"]["data"]["params"]
                params_dict = {p["key"]: p["value"] for p in params}
                model_type_name = params_dict[
                    RegisteredModelLoggedParams.MODEL_TYPE_NAME.value
                ]
                url_download_best_ckp = (
                    f"{self.credentials.server_uri}/"
                    f"get-artifact?"
                    f"path={params_dict[RegisteredModelLoggedParams.BEST_CHECKPOINT.value]}&"
                    f"run_uuid={run_id}"
                )
                logger.info(f"start download: {url_download_best_ckp} ... ")
                response_download_ckp = requests.get(
                    url_download_best_ckp, allow_redirects=True
                )
                # Need of second try block?
                try:
                    checkpoint_file_name = [
                        p["value"]
                        for p in response_params.json()["run"]["data"]["params"]
                        if p["key"] == RegisteredModelLoggedParams.BEST_CHECKPOINT.value
                    ][0]
                    checkpoint_file_path = os.path.join(
                        model_dir_path, checkpoint_file_name
                    )
                except Exception as error:
                    logger.error(error)
                    return None, None, None

                with open(checkpoint_file_path, "wb") as f:
                    f.write(response_download_ckp.content)
                    logger.info(f"Download finished: {checkpoint_file_path}")
        except Exception as e:
            logger.error(e)
            return None, None, None

        return config_file_path, checkpoint_file_path, model_type_name

    def log_model_configuration(
        self, run_id: str, configuration: BaseConfigClass
    ) -> None:
        """Log the given model configuration object as yaml file to the given
        mlflow run-id as artifact

        Args:
            run_id: The run-id the artifact should be logged to
            configuration: The configuration to log
        """
        with tempfile.TemporaryDirectory() as tmp_dir:
            config_path = Path(tmp_dir) / MlflowAdapter.__MODEL_CONFIG_FILE_NAME__
            logger.debug("Write model configuration to tmp file: %s", config_path)
            with open(config_path, "w") as tmp_config_file:
                configuration.to_yaml(
                    yaml_package=yaml,
                    dumper_cls=yaml.Dumper,
                    stream=tmp_config_file,
                    retain_collection_types=True,  # Necessary, otherwise tuples are saved as lists
                )
                self.log_artifact(run_id=run_id, local_path=tmp_config_file.name)

    def log_model_configuration_dict(
        self, run_id: str, configuration_dict: Dict[str, Any]
    ) -> None:
        """Log the given model configuration dict as yaml file to the given
        mlflow run-id as artifact

        Args:
            run_id: The run-id the artifact should be logged to
            configuration_dict: The configuration dictionary to log
        """

        with tempfile.TemporaryDirectory() as tmp_dir:
            config_path = Path(tmp_dir) / MlflowAdapter.__MODEL_CONFIG_FILE_NAME__
            logger.debug(
                "Write model configuration dictionary to tmp file: %s", config_path
            )
            with open(config_path, "w") as tmp_config_file:
                yaml.dump(
                    TupleEncoder.decode(configuration_dict),
                    stream=tmp_config_file,
                )

            self._client.log_artifact(run_id=run_id, local_path=tmp_config_file.name)
