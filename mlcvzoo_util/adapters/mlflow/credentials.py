# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining dataclasses for different credentials."""

# from __future__ import annotations

import json
import os
from enum import Enum
from typing import Any, Dict, List, Optional, Tuple

from attr import asdict, define
from related import ChildField, StringField

__error_message__: str = (
    "Necessary environment variable '{}' " "for accessing {} has not been set!"
)

from mlcvzoo_util.adapters.s3.credentials import S3Credentials


class MlflowCredentialConstants(Enum):
    """Define credentials used by MLflow."""

    AWS_SECRET_ACCESS_KEY = "AWS_SECRET_ACCESS_KEY"

    AWS_ACCESS_KEY_ID = "AWS_ACCESS_KEY_ID"

    AWS_DEFAULT_REGION = "AWS_DEFAULT_REGION"

    MLFLOW_S3_ENDPOINT_URL = "MLFLOW_S3_ENDPOINT_URL"

    MLFLOW_SERVER_URL = "MLFLOW_SERVER_URL"


@define
class MlflowCredentials:
    """Define credentials to access an MLflow server.

    Attributes:
        server_uri: Accessible URI of the MLflow server.
        s3_endpoint_url: URL of a single object in an S3 storage.
        s3_region_name: Region of the S3 storage.
        s3_artifact_access_key: Secret access key for an S3 storage.
        s3_artifact_access_id: Access ID for an S3 storage.
        run_id: ID of the associated MLflow run.
    """

    server_uri: str = StringField()
    s3_endpoint_url: str = StringField()
    s3_region_name: str = StringField()
    s3_artifact_access_key: str = StringField(repr=False)
    s3_artifact_access_id: str = StringField(repr=False)

    run_id: Optional[str] = ChildField(cls=Optional[str], default=None, required=False)

    def to_dict(self) -> Dict[str, str]:
        return asdict(self)

    @staticmethod
    def init_from_os_environment() -> "MlflowCredentials":
        """Init an MlflowCredentials object from the environment variables.

        Raises:
            ValueError: If no value for a credential constant can be found in the environment variables.

        Returns:
            The created MlflowCredentials.
        """
        module_error_string = "mlflow"

        def get_credential_constant(const: str) -> str:
            """Return value for a credential constant from the os environment variables.

            Args:
                const: Environment variable key for the constant.

            Raises:
                ValueError: If no value for a credential constant can be found in the environment variables.

            Returns:
                The value for a given constant key in the environment variables.
            """
            value = os.getenv(const)
            if value is None:
                raise ValueError(__error_message__.format(const, module_error_string))
            return value

        server_uri = get_credential_constant(
            MlflowCredentialConstants.MLFLOW_SERVER_URL.value
        )
        s3_artifact_access_key = get_credential_constant(
            MlflowCredentialConstants.AWS_SECRET_ACCESS_KEY.value
        )
        s3_artifact_access_id = get_credential_constant(
            MlflowCredentialConstants.AWS_ACCESS_KEY_ID.value
        )
        s3_endpoint_url = get_credential_constant(
            MlflowCredentialConstants.MLFLOW_S3_ENDPOINT_URL.value
        )
        s3_region_name = get_credential_constant(
            MlflowCredentialConstants.AWS_DEFAULT_REGION.value
        )

        return MlflowCredentials(
            server_uri=server_uri,
            s3_artifact_access_key=s3_artifact_access_key,
            s3_artifact_access_id=s3_artifact_access_id,
            s3_endpoint_url=s3_endpoint_url,
            s3_region_name=s3_region_name,
        )

    def update_os_environment(self) -> None:
        """Update the environment variables with the instance attributes."""
        os.environ[MlflowCredentialConstants.MLFLOW_SERVER_URL.value] = self.server_uri
        os.environ[MlflowCredentialConstants.AWS_SECRET_ACCESS_KEY.value] = (
            self.s3_artifact_access_key
        )
        os.environ[MlflowCredentialConstants.AWS_ACCESS_KEY_ID.value] = (
            self.s3_artifact_access_id
        )
        os.environ[MlflowCredentialConstants.AWS_DEFAULT_REGION.value] = (
            self.s3_region_name
        )
        os.environ[MlflowCredentialConstants.MLFLOW_S3_ENDPOINT_URL.value] = (
            self.s3_endpoint_url
        )

    def to_s3_credentials(self) -> S3Credentials:
        """Convert MlflowCredentials to S3Credentials."""
        return S3Credentials(
            s3_artifact_access_key=self.s3_artifact_access_key,
            s3_artifact_access_id=self.s3_artifact_access_id,
            s3_endpoint_url=self.s3_endpoint_url,
            s3_region_name=self.s3_region_name,
        )
