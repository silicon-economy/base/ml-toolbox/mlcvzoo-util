# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module defining dataclasses for the MlflowConnector."""

from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from typing import List


@dataclass
class ModelInfo:
    """Information of a model registered in the model registry."""

    # Dynamic attributes of a ML-Toolbox Mlflow model
    run_id: str
    name: str
    mlflow_url: str
    training_state: str
    creation_date: str
    # Attributes that are logged in the model registry with the respective name
    baseline_model: str
    algorithm_type: str
    model_type_name: str
    epochs: int
    batch_size: int
    datasets: List[str]
    evaluation_datasets: List[str]
    score: float
    description: str

    def __init__(
        self,
        run_id: str,
        name: str,
        training_state: str,
        creation_date: str,
        baseline_model: str,
        algorithm_type: str,
        model_type_name: str,
        epochs: int,
        batch_size: int,
        datasets: List[str],
        score: float,
        description: str,
        evaluation_datasets: List[str] | None = None,
        mlflow_url: str | None = None,
    ):
        """Init a ModelInfo.

        Args:
            run_id: The run id of the model
            name: The unique-name of the model
            training_state: The current training state of the model
            creation_date: The date of when the model was created
            baseline_model: The baseline model (or architecture) from which the model was derived
            algorithm_type: The algorithm used to train the model
            model_type_name: The class-type of the model that fits to a registered model in
                             the MLCVZoo registry.
            epochs: The amount of epochs a model has been trained
            batch_size: The size of sample data per epoch
            datasets: The datasets used to train the model
            score: The score of the model (defining the accuracy in some form). Since the metrics
                   have different names in different models, the score is a generic name that fits
                   to all models.
            description: The description of the model
            evaluation_datasets (Optional): The datasets used to evaluate the model
            mlflow_url (Optional): The url to the Mlflow model
        """
        self.run_id = run_id
        self.name = name
        self.training_state = training_state
        self.creation_date = creation_date
        self.baseline_model = baseline_model
        self.algorithm_type = algorithm_type
        self.model_type_name = model_type_name
        self.epochs = epochs
        self.batch_size = batch_size
        self.datasets = datasets
        self.score = score
        self.description = description
        if evaluation_datasets is None:
            self.evaluation_datasets = datasets
        else:
            self.evaluation_datasets = evaluation_datasets
        if mlflow_url is None:
            self.mlflow_url = ""
        else:
            self.mlflow_url = mlflow_url


class RegisteredModelLoggedParams(Enum):
    """Define all parameters logged for a registered model."""

    BASELINE_MODEL = "baseline_model"
    ALGORITHM_TYPE = "algorithm_type"
    MODEL_TYPE_NAME = "model_type_name"
    EPOCHS = "epochs"
    BATCH_SIZE = "batch_size"
    DATASETS = "datasets"
    EVALUATION_DATASETS = "evaluation_datasets"
    SCORE = "score"
    DESCRIPTION = "description"
    BEST_CHECKPOINT = "best_checkpoint"
    BEST_CHECKPOINT_STEP = "best_checkpoint_step"


class TrainingStatus(Enum):
    """Enum for all available training run states."""

    NEW = "NEW"
    RUNNING = "RUNNING"
    FINISHED = "FINISHED"
    CRASHED = "CRASHED"
