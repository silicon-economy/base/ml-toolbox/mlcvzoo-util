# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Module that defines frequently used functions."""

import os
from pathlib import Path
from typing import AnyStr


def get_root_dir(file_path: AnyStr, file_depth: int) -> str:
    """Get root directory i.e. the absolute path of the python backend.

    Args:
        file_path: Path of file for which to get the root dir.
        file_depth: Depth of given file.

    Returns:
        Absolute path of the root directory.
    """
    return str(Path(str(os.path.dirname(file_path))).parents[file_depth])
