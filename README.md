# MLCVZoo Util

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_util** provides additional utility functions for downstream developments and tools for convenience and CLI usage.

## Install
`
pip install mlcvzoo-util
`

## Technology stack

- Python
